package terra.common.api.dto.reports.registry;

import java.io.Serializable;


public class RegistryReportItemDTO implements Serializable {
    private final String address;
    private final Long orderCode;
    private final Double weight;
    private final Double orderCost;
    private final String desiredTime;

    public RegistryReportItemDTO(String address, Long orderCode, Double weight, Double orderCost, String desiredTime) {
        this.address = address;
        this.orderCode = orderCode;
        this.weight = weight;
        this.orderCost = orderCost;
        this.desiredTime = desiredTime;
    }

    public String getAddress() {
        return address;
    }

    public Long getOrderCode() {
        return orderCode;
    }

    public Double getWeight() {
        return weight;
    }

    public Double getOrderCost() {
        return orderCost;
    }

    public String getDesiredTime() {
        return desiredTime;
    }

    @Override
    public String toString() {
        return "RegistryReportDTO{" +
                "address='" + address + '\'' +
                ", orderCode=" + orderCode +
                ", weight=" + weight +
                ", orderCost=" + orderCost +
                ", desiredTime='" + desiredTime + '\'' +
                '}';
    }
}
