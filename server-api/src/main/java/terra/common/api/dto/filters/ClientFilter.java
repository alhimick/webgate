package terra.common.api.dto.filters;


import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

public class ClientFilter extends BaseFilter {
    private static final List<String> NAME_FIELDS = Arrays.asList("clients.first_name", "clients.second_name", "clients.surname");
    private String name;
    private String address;
    private String phone;
    private String possibleGoods;
    private String pet;
    private Long id;
    private String petSpecies;
    private String petName;
    private Long internetCode;


    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setPossibleGoods(String possibleGoods) {
        this.possibleGoods = possibleGoods;
    }

    public void setPet(String pet) {
        this.pet = pet;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setPetSpecies(String petSpecies) {
        this.petSpecies = petSpecies;
    }

    public void setPetName(String petName) {
        this.petName = petName;
    }

    public void setInternetCode(Long internetCode) {
        this.internetCode = internetCode;
    }

    @Override
    public String getFilterString() {
        StringBuilder result = new StringBuilder();
        addMultiStringFilter(result, NAME_FIELDS, name);
        addStringFilter(result, "clients.address", address);
        addStringFilter(result, "clients.phone", phone);
        addStringFilter(result, "clients.possibly_goods", possibleGoods);
        addStringFilter(result, "clients.pet", pet);
        addLongFilter(result, "clients.id", id);
        addStringFilter(result, "salon_pet.pet_species", petSpecies);
        addStringFilter(result, "salon_pet.pet_name", petName);
        addLongFilter(result, "clients.internet_code", internetCode);
        return result.toString().isEmpty() ? " " : result.toString();
    }

    @Override
    public int setupStatement(PreparedStatement ps, int index) throws SQLException {
        for (int i = 0; i < NAME_FIELDS.size(); i++) {
            index = setupStringPS(ps, index, name, false);
        }
        index = setupStringPS(ps, index, address, false);
        index = setupStringPS(ps, index, phone, false);
        index = setupStringPS(ps, index, possibleGoods, false);
        index = setupStringPS(ps, index, pet, false);
        index = setupLongPS(ps, index, id);
        index = setupStringPS(ps, index, petSpecies, false);
        index = setupStringPS(ps, index, petName, false);
        index = setupLongPS(ps, index, internetCode);
        return index;
    }

    @Override
    public String toString() {
        return "ClientFilter{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", phone='" + phone + '\'' +
                ", possibleGoods='" + possibleGoods + '\'' +
                ", pet='" + pet + '\'' +
                ", id=" + id +
                ", petSpecies='" + petSpecies + '\'' +
                ", petName='" + petName + '\'' +
                ", internetCode=" + internetCode +
                "} " + super.toString();
    }
}
