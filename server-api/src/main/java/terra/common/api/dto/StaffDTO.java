package terra.common.api.dto;


import java.io.Serializable;

public class StaffDTO implements Serializable, Identifiable, Nameful {
    private final long id;
    private final String name;
    private final String phone;
    private final boolean isWorking;

    public StaffDTO(long id, String name, String phone, boolean isWorking) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.isWorking = isWorking;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public boolean isWorking() {
        return isWorking;
    }

    @Override
    public String toString() {
        return "StaffDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", isWorking=" + isWorking +
                '}';
    }

    public enum Type {DRIVER, MANAGER, MASTER}
}
