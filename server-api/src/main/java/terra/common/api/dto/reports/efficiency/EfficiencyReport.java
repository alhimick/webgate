package terra.common.api.dto.reports.efficiency;

import terra.common.api.dto.reports.SimpleReport;
import terra.common.api.dto.reports.TeraTableModel;
import terra.common.utils.TeraMath;

import javax.swing.table.TableModel;
import java.sql.Date;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class EfficiencyReport implements SimpleReport {
    private final Map<String, Object> fields = new HashMap<>();
    private final TableModel model;

    public EfficiencyReport(Date since, Date till, String status, List<EfficiencyReportItemDTO> items) {
        fields.put("since", since.toString());
        fields.put("till", till.toString());
        fields.put("status", status == null ? "" : status);
        List<List<String>> data = items.stream().map(item -> Arrays.asList(item.getName(),
                Double.toString(TeraMath.round(item.getAvgBill())) + "р.",
                Double.toString(TeraMath.round(item.getTotalBill())) + "р.",
                Integer.toString(item.getAmount()))
        ).collect(Collectors.toList());
        model = new TeraTableModel(Arrays.asList("name", "avgBill", "totalBill", "amount"), data);
    }

    @Override
    public Map<String, Object> getFields() {
        return fields;
    }

    @Override
    public TableModel getTableModel() {
        return model;
    }
}
