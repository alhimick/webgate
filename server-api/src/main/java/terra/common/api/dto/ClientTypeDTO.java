package terra.common.api.dto;


import java.io.Serializable;

public class ClientTypeDTO implements Serializable, Identifiable, Nameful {
    private final long id;
    private final String name;

    public ClientTypeDTO(long id, String type) {
        this.id = id;
        this.name = type;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "ClientTypeDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
