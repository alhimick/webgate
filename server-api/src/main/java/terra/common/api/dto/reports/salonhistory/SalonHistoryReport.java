package terra.common.api.dto.reports.salonhistory;

import terra.common.api.dto.SalonOrderDTO;
import terra.common.api.dto.SalonPetDTO;
import terra.common.api.dto.reports.SimpleReport;
import terra.common.api.dto.reports.TeraTableModel;
import terra.common.utils.TeraMath;

import javax.swing.table.TableModel;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class SalonHistoryReport implements SimpleReport {
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy");
    private final Map<String, Object> fields = new HashMap<>();
    private final TableModel model;

    public SalonHistoryReport(List<SalonOrderDTO> reportData, SalonPetDTO pet) {
        fields.put("pet", pet.getName());
        fields.put("petType", pet.getType().getName() + " " + pet.getSpecies());
        fields.put("petNotes", pet.getNotes());
        fields.put("owner", pet.getOwner().getName());
        fields.put("ownerId", Long.toString(pet.getOwner().getId()));
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        fields.put("age", Integer.toString(currentYear - pet.getBirth()) + "(лет)");
        String gender = pet.getGender() == SalonPetDTO.Gender.MALE ? "М" :
                pet.getGender() == SalonPetDTO.Gender.FEMALE ? "Ж" : "";
        fields.put("gender", gender);

        List<List<String>> data = reportData.stream().map(item -> Arrays.asList(
                DATE_FORMAT.format(item.getStart()), item.getMaster().getName(),
                Double.toString(TeraMath.round(item.getCost())), item.isCanceled() ? "X" : "",
                item.getNotes())
        ).collect(Collectors.toList());
        model = new TeraTableModel(Arrays.asList("date", "master", "cost", "canceled", "description"), data);
    }

    @Override
    public Map<String, Object> getFields() {
        return fields;
    }

    @Override
    public TableModel getTableModel() {
        return model;
    }
}
