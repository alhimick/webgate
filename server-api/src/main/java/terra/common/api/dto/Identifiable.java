package terra.common.api.dto;


public interface Identifiable {
    long getId();
}
