package terra.common.api.dto;


import java.io.Serializable;

public class SalonPetDTO implements Identifiable, Nameful, Serializable {
    private final long id;
    private final long ownerId;
    private final String name;
    private final String species;
    private final int birth;
    private final Gender gender;
    private final long petTypeId;
    private final String notes;
    private ClientDTO owner;
    private PetTypeDTO type;

    public SalonPetDTO(long id, long ownerId, String name, String species, int birth, Gender gender, long petTypeId, String notes) {
        this.id = id;
        this.ownerId = ownerId;
        this.name = name;
        this.species = species;
        this.birth = birth;
        this.gender = gender;
        this.petTypeId = petTypeId;
        this.notes = notes;
    }

    @Override
    public long getId() {
        return id;
    }

    public long getOwnerId() {
        return ownerId;
    }

    @Override
    public String getName() {
        return name;
    }

    public String getSpecies() {
        return species;
    }

    public int getBirth() {
        return birth;
    }

    public Gender getGender() {
        return gender;
    }

    public long getPetTypeId() {
        return petTypeId;
    }

    public String getNotes() {
        return notes;
    }

    public ClientDTO getOwner() {
        return owner;
    }

    public void setOwner(ClientDTO owner) {
        this.owner = owner;
    }

    public PetTypeDTO getType() {
        return type;
    }

    public void setType(PetTypeDTO type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "SalonPetDTO{" +
                "id=" + id +
                ", ownerId=" + ownerId +
                ", name='" + name + '\'' +
                ", species='" + species + '\'' +
                ", birth=" + birth +
                ", gender=" + gender +
                ", petTypeId=" + petTypeId +
                ", notes='" + notes + '\'' +
                ", owner=" + owner +
                ", type=" + type +
                '}';
    }

    public enum Gender {MALE, FEMALE, NONE}
}
