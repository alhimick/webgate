package terra.common.api.dto;


import terra.common.utils.TeraMath;

import java.util.Objects;

public interface Diffable<T extends Diffable<?>> extends Identifiable {
    String getDiff(T another);

    default void diff(Object first, Object second, StringBuilder sb, String description) {
        if (!Objects.equals(first, second)) {
            String firstStr = first == null || Objects.equals(first.toString(), "") ? "<empty>" : first.toString();
            String secondStr = second == null || Objects.equals(second.toString(), "") ? "<empty>" : second.toString();
            sb.append(description).append(":").append(firstStr).append("->").append(secondStr).append(" ").append("\n");
        }
    }

    default void diff(boolean first, boolean second, StringBuilder sb, String description) {
        if (first != second)
            sb.append(description).append(":").append(first).append("->").append(second).append(" ").append("\n");
    }

    default void diff(double first, double second, StringBuilder sb, String description) {
        if (!TeraMath.equals(first, second))
            sb.append(description).append(":").append(first).append("->").append(second).append(" ").append("\n");
    }

    default void diff(int first, int second, StringBuilder sb, String description) {
        if (first != second)
            sb.append(description).append(":").append(first).append("->").append(second).append(" ").append("\n");
    }
}
