package terra.common.api.dto.reports;


import terra.common.api.dto.OrderDTO;
import terra.common.api.dto.OrderPositionDTO;
import terra.common.utils.RussianMoney;
import terra.common.utils.TeraMath;

import javax.swing.table.TableModel;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class BillReport implements SimpleReport {
    private final Map<String, Object> billFields = new HashMap<>();
    private final TableModel model;

    public BillReport(OrderDTO order, List<OrderPositionDTO> positions) {
        billFields.put("client", order.getClient().getName());
        billFields.put("address", order.getAddress());
        billFields.put("phone", order.getPhone() == null ? order.getClient().getPhone() : order.getPhone());
        billFields.put("desiredTime", order.getDesiredTime() == null ? "" : order.getDesiredTime());
        billFields.put("deliveryDate", order.getDeliveryTime());
        String orderCode = Long.toString(order.getId()) + (order.isInternet() ? "-" + order.getInternetCode() : "");
        billFields.put("orderCode", orderCode);
        billFields.put("orderId", order.getId());

        List<List<String>> data = positions.stream().map(position -> Arrays.asList(
                position.getArticle() == null ? "" : position.getArticle(), position.getName(),
                Double.toString(position.getPrice()), Integer.toString(position.getCount()),
                Double.toString(position.getDiscount()), Integer.toString((int) getCost(position)),
                Double.toString(position.getWeight()))
        ).collect(Collectors.toList());
        model = new TeraTableModel(Arrays.asList("article", "name", "price", "count", "discount", "cost", "weight"), data);
        double totalCost = positions.stream().mapToDouble(this::getCost).sum();
        billFields.put("totalCost", Double.toString(TeraMath.roundToRoubles(totalCost)));
        billFields.put("totalCostText", RussianMoney.digits2Text(TeraMath.roundToRoubles(totalCost)));
        billFields.put("manager", order.getManager().getName());
        billFields.put("driver", order.getDriver().getName());
    }

    private double getCost(OrderPositionDTO position) {
        return TeraMath.resultPrice(position.getPrice(), position.getDiscount(), position.getCount());
    }

    @Override
    public Map<String, Object> getFields() {
        return billFields;
    }

    @Override
    public TableModel getTableModel() {
        return model;
    }
}
