package terra.common.api.dto;


import java.io.Serializable;
import java.sql.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class OrderDTO implements Serializable, Identifiable, Diffable<OrderDTO> {
    private final long id;
    private final String internetCode;
    private final long clientId;
    private final Date date;
    private final String address;
    private final long managerId;
    private final long driverId;
    private final String desiredTime;
    private final Date deliveryTime;
    private final String description;
    private final long clientTypeId;
    private final long orderStateId;
    private final String phone;
    private final String possibleGoods;
    private ClientDTO client;
    private OrderStateDTO state;
    private StaffDTO manager;
    private StaffDTO driver;
    private List<OrderPositionDTO> positions;

    private OrderDTO(Builder builder) {
        this.id = builder.id;
        this.internetCode = builder.internetCode;
        this.clientId = builder.clientId;
        this.date = builder.date;
        this.address = builder.address;
        this.managerId = builder.managerId;
        this.driverId = builder.driverId;
        this.desiredTime = builder.desiredTime;
        this.deliveryTime = builder.deliveryTime;
        this.description = builder.description;
        this.clientTypeId = builder.clientTypeId;
        this.orderStateId = builder.orderStateId;
        this.phone = builder.phone;
        this.possibleGoods = builder.possibleGoods;
        this.client = builder.client;
        this.state = builder.state;
        this.manager = builder.manager;
        this.driver = builder.driver;
        this.positions = builder.positions;
    }

    public static Builder newOrder() {
        return new Builder();
    }

    public Builder copyOrder() {
        return new Builder(this);
    }

    public long getId() {
        return id;
    }

    public String getInternetCode() {
        return internetCode;
    }

    public boolean isInternet() {
        return internetCode != null && !internetCode.isEmpty();
    }

    public long getClientId() {
        return clientId;
    }

    public Date getDate() {
        return date;
    }

    public String getAddress() {
        return address;
    }

    public long getManagerId() {
        return managerId;
    }

    public long getDriverId() {
        return driverId;
    }

    public String getDesiredTime() {
        return desiredTime;
    }

    public Date getDeliveryTime() {
        return deliveryTime;
    }

    public String getDescription() {
        return description;
    }

    public long getClientTypeId() {
        return clientTypeId;
    }

    public long getOrderStateId() {
        return orderStateId;
    }

    public String getPhone() {
        return phone;
    }

    public String getPossibleGoods() {
        return possibleGoods;
    }

    public ClientDTO getClient() {
        return client;
    }

    public void setClient(ClientDTO client) {
        this.client = client;
    }

    public OrderStateDTO getState() {
        return state;
    }

    public void setState(OrderStateDTO state) {
        this.state = state;
    }

    public StaffDTO getManager() {
        return manager;
    }

    public void setManager(StaffDTO manager) {
        this.manager = manager;
    }

    public StaffDTO getDriver() {
        return driver;
    }

    public void setDriver(StaffDTO driver) {
        this.driver = driver;
    }

    public List<OrderPositionDTO> getPositions() {
        return positions;
    }

    public void setPositions(List<OrderPositionDTO> positions) {
        this.positions = positions;
    }

    @Override
    public String toString() {
        return "OrderDTO{" +
                "id=" + id +
                ", internetCode='" + internetCode + '\'' +
                ", clientId=" + clientId +
                ", date=" + date +
                ", address='" + address + '\'' +
                ", managerId=" + managerId +
                ", driverId=" + driverId +
                ", desiredTime='" + desiredTime + '\'' +
                ", deliveryTime=" + deliveryTime +
                ", description='" + description + '\'' +
                ", clientTypeId=" + clientTypeId +
                ", orderStateId=" + orderStateId +
                ", phone='" + phone + '\'' +
                ", possibleGoods='" + possibleGoods + '\'' +
                ", client=" + client +
                ", state=" + state +
                ", manager=" + manager +
                ", driver=" + driver +
                ", positions=" + positions +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OrderDTO orderDTO = (OrderDTO) o;

        if (id != orderDTO.id) return false;
        if (clientId != orderDTO.clientId) return false;
        if (managerId != orderDTO.managerId) return false;
        if (driverId != orderDTO.driverId) return false;
        if (clientTypeId != orderDTO.clientTypeId) return false;
        if (orderStateId != orderDTO.orderStateId) return false;
        if (!Objects.equals(internetCode, orderDTO.internetCode)) return false;
        if (!date.equals(orderDTO.date)) return false;
        if (!address.equals(orderDTO.address)) return false;
        if (!Objects.equals(desiredTime, orderDTO.desiredTime)) return false;
        if (!Objects.equals(deliveryTime, orderDTO.deliveryTime)) return false;
        if (!Objects.equals(description, orderDTO.description)) return false;
        if (!Objects.equals(phone, orderDTO.phone)) return false;
        if (positions != null && orderDTO.positions != null) {
            Set<OrderPositionDTO> existing = new HashSet<>(positions);
            existing.removeAll(orderDTO.positions);
            Set<OrderPositionDTO> other = new HashSet<>(orderDTO.positions);
            existing.removeAll(positions);
            return existing.isEmpty() && other.isEmpty();
        }
        return Objects.equals(possibleGoods, orderDTO.possibleGoods);

    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (internetCode != null ? internetCode.hashCode() : 0);
        result = 31 * result + (int) (clientId ^ (clientId >>> 32));
        result = 31 * result + date.hashCode();
        result = 31 * result + address.hashCode();
        result = 31 * result + (int) (managerId ^ (managerId >>> 32));
        result = 31 * result + (int) (driverId ^ (driverId >>> 32));
        result = 31 * result + (desiredTime != null ? desiredTime.hashCode() : 0);
        result = 31 * result + (deliveryTime != null ? deliveryTime.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (int) (clientTypeId ^ (clientTypeId >>> 32));
        result = 31 * result + (int) (orderStateId ^ (orderStateId >>> 32));
        result = 31 * result + phone.hashCode();
        result = 31 * result + (possibleGoods != null ? possibleGoods.hashCode() : 0);
        return result;
    }

    @Override
    public String getDiff(OrderDTO another) {
        StringBuilder sb = new StringBuilder();
        diff(internetCode, another.internetCode, sb, "Интернет код");
        diff(client.getName(), another.client.getName(), sb, "Клиент");
        diff(date, another.date, sb, "Дата");
        diff(address, another.address, sb, "Адрес");
        diff(manager.getName(), another.manager.getName(), sb, "Продавец");
        diff(driver.getName(), another.driver.getName(), sb, "Водитель");
        diff(desiredTime, another.desiredTime, sb, "Желаемое время");
        diff(deliveryTime, another.deliveryTime, sb, "Дата доставки");
        diff(description, another.description, sb, "Описание");
        diff(client.getType().getName(), another.client.getType().getName(), sb, "Тип клиента");
        diff(state.getName(), another.state.getName(), sb, "Статус");
        diff(phone, another.phone, sb, "Телефон");
        diff(possibleGoods, another.possibleGoods, sb, "Возм. товар");
        return sb.toString();
    }

    public static final class Builder {
        private long id;
        private String internetCode;
        private long clientId;
        private Date date;
        private String address;
        private long managerId;
        private long driverId;
        private String desiredTime;
        private Date deliveryTime;
        private String description;
        private long clientTypeId;
        private long orderStateId;
        private String phone;
        private String possibleGoods;
        private ClientDTO client;
        private OrderStateDTO state;
        private StaffDTO manager;
        private StaffDTO driver;
        private List<OrderPositionDTO> positions;

        private Builder() {
        }

        private Builder(OrderDTO o) {
            this.id = o.id;
            this.internetCode = o.internetCode;
            this.clientId = o.clientId;
            this.date = o.date;
            this.address = o.address;
            this.managerId = o.managerId;
            this.driverId = o.driverId;
            this.desiredTime = o.desiredTime;
            this.deliveryTime = o.deliveryTime;
            this.description = o.description;
            this.clientTypeId = o.clientTypeId;
            this.orderStateId = o.orderStateId;
            this.phone = o.phone;
            this.possibleGoods = o.possibleGoods;
            this.manager = o.manager;
            this.driver = o.driver;
            this.client = o.client;
            this.state = o.state;
            this.positions = o.positions;
        }

        public OrderDTO build() {
            return new OrderDTO(this);
        }

        public Builder id(long id) {
            this.id = id;
            return this;
        }

        public Builder internetCode(String internetCode) {
            this.internetCode = internetCode;
            return this;
        }

        public Builder clientId(long clientId) {
            this.clientId = clientId;
            return this;
        }

        public Builder date(Date date) {
            this.date = date;
            return this;
        }

        public Builder address(String address) {
            this.address = address;
            return this;
        }

        public Builder managerId(long managerId) {
            this.managerId = managerId;
            return this;
        }

        public Builder driverId(long driverId) {
            this.driverId = driverId;
            return this;
        }

        public Builder desiredTime(String desiredTime) {
            this.desiredTime = desiredTime;
            return this;
        }

        public Builder deliveryTime(Date deliveryTime) {
            this.deliveryTime = deliveryTime;
            return this;
        }

        public Builder description(String description) {
            this.description = description;
            return this;
        }

        public Builder clientTypeId(long clientTypeId) {
            this.clientTypeId = clientTypeId;
            return this;
        }

        public Builder orderStateId(long orderStateId) {
            this.orderStateId = orderStateId;
            return this;
        }

        public Builder phone(String phone) {
            this.phone = phone;
            return this;
        }

        public Builder possibleGoods(String possibleGoods) {
            this.possibleGoods = possibleGoods;
            return this;
        }

        public Builder client(ClientDTO client) {
            this.client = client;
            return this;
        }

        public Builder state(OrderStateDTO state) {
            this.state = state;
            return this;
        }

        public Builder manager(StaffDTO manager) {
            this.manager = manager;
            return this;
        }

        public Builder driver(StaffDTO driver) {
            this.driver = driver;
            return this;
        }

        public Builder positions(List<OrderPositionDTO> positions) {
            this.positions = positions;
            return this;
        }
    }
}
