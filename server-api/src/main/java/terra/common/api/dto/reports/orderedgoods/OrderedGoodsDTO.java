package terra.common.api.dto.reports.orderedgoods;


import java.io.Serializable;

public class OrderedGoodsDTO implements Serializable {
    private final String article;
    private final String name;
    private final int totalCount;
    private final double totalCost;
    private final double totalWeight;
    private final String orders;

    public OrderedGoodsDTO(String article, String name, int totalCount, double totalCost, double totalWeight, String orders) {
        this.article = article;
        this.name = name;
        this.totalCount = totalCount;
        this.totalCost = totalCost;
        this.totalWeight = totalWeight;
        this.orders = orders;
    }

    public String getArticle() {
        return article;
    }

    public String getName() {
        return name;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public double getTotalCost() {
        return totalCost;
    }

    public String getOrders() {
        return orders;
    }

    public double getTotalWeight() {
        return totalWeight;
    }

    @Override
    public String toString() {
        return "OrderedGoodsDTO{" +
                "article='" + article + '\'' +
                ", name='" + name + '\'' +
                ", totalCount=" + totalCount +
                ", totalCost=" + totalCost +
                ", totalWeight=" + totalWeight +
                ", orders='" + orders + '\'' +
                '}';
    }
}
