package terra.common.api.dto.reports.registry;


import terra.common.api.dto.reports.SimpleReport;
import terra.common.api.dto.reports.TeraTableModel;
import terra.common.utils.TeraMath;

import javax.swing.table.TableModel;
import java.sql.Date;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class RegistryReport implements SimpleReport {
    private final Map<String, Object> fields = new HashMap<>();
    private final TableModel model;

    public RegistryReport(List<RegistryReportItemDTO> reportData, String driverName, Date date) {
        fields.put("driverName", driverName);
        fields.put("date", date.toString());
        List<List<String>> data = reportData.stream().map(item -> Arrays.asList(item.getAddress(),
                item.getDesiredTime(), Long.toString(item.getOrderCode()),
                Double.toString(TeraMath.round(item.getWeight())) + "кг",
                Double.toString(TeraMath.roundToRoubles(item.getOrderCost())) + "р.")
        ).collect(Collectors.toList());
        double totalWeight = reportData.stream().mapToDouble(RegistryReportItemDTO::getWeight).sum();
        double totalCost = reportData.stream().mapToDouble(RegistryReportItemDTO::getOrderCost).sum();
        fields.put("totalWeight", Double.toString(TeraMath.round(totalWeight)) + "кг");
        fields.put("totalCost", Double.toString(TeraMath.roundToRoubles(totalCost)) + "р.");
        model = new TeraTableModel(Arrays.asList("address", "desiredTime", "orderCode", "weight", "orderCost"), data);
    }

    @Override
    public Map<String, Object> getFields() {
        return fields;
    }

    @Override
    public TableModel getTableModel() {
        return model;
    }
}
