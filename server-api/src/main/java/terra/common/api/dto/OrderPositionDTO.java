package terra.common.api.dto;


import java.io.Serializable;
import java.util.Objects;

public class OrderPositionDTO implements Serializable, Nameful, Diffable<OrderPositionDTO> {
    private final long id;
    private final long orderCode;
    private final String name;
    private final double price;
    private final double weight;
    private final double discount;
    private final int count;
    private final String article;
    private final String metadata;
    private final String additionalArticles;


    private OrderPositionDTO(Builder builder) {
        this.id = builder.id;
        this.orderCode = builder.orderCode;
        this.name = builder.name;
        this.price = builder.price;
        this.weight = builder.weight;
        this.discount = builder.discount;
        this.count = builder.count;
        this.article = builder.article;
        this.metadata = builder.metadata;
        this.additionalArticles = builder.additionalArticles;
    }

    public static Builder newOrderPosition() {
        return new Builder();
    }

    public Builder copyPosition() {
        return new Builder(this);
    }

    public long getId() {
        return id;
    }

    public long getOrderCode() {
        return orderCode;
    }

    @Override
    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public double getWeight() {
        return weight;
    }

    public double getDiscount() {
        return discount;
    }

    public int getCount() {
        return count;
    }

    public String getArticle() {
        return article;
    }

    public String getMetadata() {
        return metadata;
    }

    public String getAdditionalArticles() {
        return additionalArticles;
    }

    @Override
    public String toString() {
        return "OrderPositionDTO{" +
                "id=" + id +
                ", orderCode=" + orderCode +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", weight=" + weight +
                ", discount=" + discount +
                ", count=" + count +
                ", article='" + article + '\'' +
                ", metadata='" + metadata + '\'' +
                ", additionalArticles='" + additionalArticles + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) return false;
        OrderPositionDTO that = (OrderPositionDTO) o;
        return equalsInet(o) && Objects.equals(additionalArticles, that.additionalArticles) &&
                Objects.equals(metadata, that.metadata);
    }

    public boolean equalsInet(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderPositionDTO that = (OrderPositionDTO) o;
        return id == that.id &&
                orderCode == that.orderCode &&
                Double.compare(that.price, price) == 0 &&
                Double.compare(that.weight, weight) == 0 &&
                Double.compare(that.discount, discount) == 0 &&
                count == that.count &&
                Objects.equals(name, that.name) &&
                Objects.equals(article, that.article);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getOrderCode(), getName(), getPrice(), getWeight(), getDiscount(), getCount());
    }

    @Override
    public String getDiff(OrderPositionDTO another) {
        StringBuilder sb = new StringBuilder();
        diff(name, another.name, sb, "Название");
        diff(price, another.price, sb, "Цена");
        diff(weight, another.weight, sb, "Вес");
        diff(discount, another.discount, sb, "Скидка");
        diff(count, another.count, sb, "Количество");
        diff(article, another.article, sb, "Артикул");
        diff(metadata, another.metadata, sb, "Метадата");
        diff(additionalArticles, another.additionalArticles, sb, "Доп артикли");
        return sb.toString();
    }

    public static final class Builder {
        private long id;
        private long orderCode;
        private String name;
        private double price;
        private double weight;
        private double discount;
        private int count;
        private String article;
        private String metadata;
        private String additionalArticles;

        private Builder() {
        }

        private Builder(OrderPositionDTO p) {
            this.id = p.id;
            this.orderCode = p.orderCode;
            this.name = p.name;
            this.price = p.price;
            this.weight = p.weight;
            this.discount = p.discount;
            this.count = p.count;
            this.article = p.article;
            this.metadata = p.metadata;
            this.additionalArticles = p.additionalArticles;
        }

        public OrderPositionDTO build() {
            return new OrderPositionDTO(this);
        }

        public Builder id(long id) {
            this.id = id;
            return this;
        }

        public Builder orderCode(long orderCode) {
            this.orderCode = orderCode;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder price(double price) {
            this.price = price;
            return this;
        }

        public Builder weight(double weight) {
            this.weight = weight;
            return this;
        }

        public Builder discount(double discount) {
            this.discount = discount;
            return this;
        }

        public Builder count(int count) {
            this.count = count;
            return this;
        }

        public Builder article(String article) {
            this.article = article;
            return this;
        }

        public Builder metadata(String metadata) {
            this.metadata = metadata;
            return this;
        }

        public Builder additionalArticles(String additionalArticles) {
            this.additionalArticles = additionalArticles;
            return this;
        }
    }
}
