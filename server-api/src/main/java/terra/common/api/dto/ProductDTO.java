package terra.common.api.dto;


import java.io.Serializable;
import java.util.Collections;
import java.util.List;

public class ProductDTO implements Identifiable, Nameful, Serializable {
    private final long id;
    private final String name;
    private final List<Ext> variants;

    public ProductDTO(long id, String name, List<Ext> variants) {
        this.id = id;
        this.name = name;
        this.variants = variants;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    public List<Ext> getVariants() {
        return Collections.unmodifiableList(variants);
    }

    @Override
    public String toString() {
        return "ProductDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", variants=" + variants +
                '}';
    }

    public static class Ext implements Identifiable, Nameful, Serializable {
        private final long id;
        private final long productId;
        private final String name;
        private final double price;
        private final double weight;
        private final String article;
        private final String additionalArticles;

        public Ext(long id, long productId, String name, double price, double weight, String article,
                   String additionalArticles) {
            this.id = id;
            this.productId = productId;
            this.name = name;
            this.price = price;
            this.weight = weight;
            this.article = article;
            this.additionalArticles = additionalArticles;
        }

        public long getId() {
            return id;
        }

        public long getProductId() {
            return productId;
        }

        public String getName() {
            return name;
        }

        public double getPrice() {
            return price;
        }

        public double getWeight() {
            return weight;
        }

        public String getArticle() {
            return article;
        }

        public String getAdditionalArticles() {
            return additionalArticles;
        }

        @Override
        public String toString() {
            return "Ext{" +
                    "id=" + id +
                    ", productId=" + productId +
                    ", name='" + name + '\'' +
                    ", price=" + price +
                    ", weight=" + weight +
                    ", article='" + article + '\'' +
                    ", additionalArticles='" + additionalArticles + '\'' +
                    '}';
        }
    }
}
