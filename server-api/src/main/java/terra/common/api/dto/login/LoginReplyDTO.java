package terra.common.api.dto.login;


import terra.common.api.permissions.Roles;

import java.io.Serializable;

public class LoginReplyDTO implements Serializable {
    private final String sessionId;
    private final Roles roles;

    public LoginReplyDTO(String sessionId, Roles roles) {
        this.sessionId = sessionId;
        this.roles = roles;
    }

    public String getSessionId() {
        return sessionId;
    }

    public Roles getRoles() {
        return roles;
    }

    @Override
    public String toString() {
        return "LoginReplyDTO{" +
                "sessionId='" + sessionId + '\'' +
                ", roles=" + roles +
                '}';
    }
}
