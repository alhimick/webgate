package terra.common.api.dto.reports.clientreport;


import java.io.Serializable;

public class ClientReportItemDTO implements Serializable {
    private final String firstName;
    private final String secondName;
    private final String surname;
    private final String email;
    private final String phone;

    public ClientReportItemDTO(String surname, String firstName, String secondName, String email, String phone) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.surname = surname;
        this.email = email;
        this.phone = phone;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public String getSurname() {
        return surname;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    @Override
    public String toString() {
        return "ClientReportItemDTO{" +
                "firstName='" + firstName + '\'' +
                ", secondName='" + secondName + '\'' +
                ", surname='" + surname + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}
