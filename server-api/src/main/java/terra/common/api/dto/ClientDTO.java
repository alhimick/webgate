package terra.common.api.dto;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ClientDTO implements Serializable, Identifiable, Nameful, Diffable<ClientDTO> {

    private final long id;
    private final String firstName;
    private final String secondName;
    private final String surname;
    private final String phone;
    private final String email;
    private final long typeId;
    private final String possibleGoods;
    private final String pet;
    private final String address;
    private final String notes;
    private final boolean issues;
    private final Long internetCode;
    private ClientTypeDTO type;
    private List<SalonPetDTO> pets;

    public ClientDTO(long id, String firstName, String secondName, String surname, String phone, String email,
                     long typeId, String possibleGoods, String pet, String address, String notes, boolean issues,
                     Long internetCode, ClientTypeDTO type) {
        this.id = id;
        this.firstName = firstName;
        this.secondName = secondName;
        this.surname = surname;
        this.phone = phone;
        this.email = email;
        this.typeId = typeId;
        this.possibleGoods = possibleGoods;
        this.pet = pet;
        this.address = address;
        this.notes = notes;
        this.issues = issues;
        this.type = type;
        this.internetCode = internetCode;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        StringBuilder result = new StringBuilder(firstName);
        if (secondName != null && !secondName.isEmpty())
            result.append(" ").append(secondName);
        if (surname != null && !surname.isEmpty())
            result.append(" ").append(surname);
        return result.toString();
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public String getSurname() {
        return surname;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public long getTypeId() {
        return typeId;
    }

    public String getPossibleGoods() {
        return possibleGoods;
    }

    public String getPet() {
        return pet;
    }

    public String getAddress() {
        return address;
    }

    public String getNotes() {
        return notes;
    }

    public boolean isIssues() {
        return issues;
    }

    public ClientTypeDTO getType() {
        return type;
    }

    public void setType(ClientTypeDTO type) {
        this.type = type;
    }

    public List<SalonPetDTO> getPets() {
        return pets == null ? pets = new ArrayList<>() : pets;
    }

    public Long getInternetCode() {
        return internetCode;
    }

    @Override
    public String toString() {
        return "ClientDTO{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", secondName='" + secondName + '\'' +
                ", surname='" + surname + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", typeId=" + typeId +
                ", possibleGoods='" + possibleGoods + '\'' +
                ", pet='" + pet + '\'' +
                ", address='" + address + '\'' +
                ", notes='" + notes + '\'' +
                ", issues=" + issues +
                ", internetCode=" + internetCode +
                ", type=" + type +
                ", pets=" + pets +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ClientDTO clientDTO = (ClientDTO) o;

        if (id != clientDTO.id) return false;
        if (typeId != clientDTO.typeId) return false;
        if (issues != clientDTO.issues) return false;
        if (firstName != null ? !firstName.equals(clientDTO.firstName) : clientDTO.firstName != null) return false;
        if (secondName != null ? !secondName.equals(clientDTO.secondName) : clientDTO.secondName != null) return false;
        if (surname != null ? !surname.equals(clientDTO.surname) : clientDTO.surname != null) return false;
        if (phone != null ? !phone.equals(clientDTO.phone) : clientDTO.phone != null) return false;
        if (email != null ? !email.equals(clientDTO.email) : clientDTO.email != null) return false;
        if (possibleGoods != null ? !possibleGoods.equals(clientDTO.possibleGoods) : clientDTO.possibleGoods != null)
            return false;
        if (pet != null ? !pet.equals(clientDTO.pet) : clientDTO.pet != null) return false;
        if (address != null ? !address.equals(clientDTO.address) : clientDTO.address != null) return false;
        if (notes != null ? !notes.equals(clientDTO.notes) : clientDTO.notes != null) return false;
        return internetCode != null ? !internetCode.equals(clientDTO.internetCode) : clientDTO.internetCode != null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (secondName != null ? secondName.hashCode() : 0);
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + (phone != null ? phone.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (int) (typeId ^ (typeId >>> 32));
        result = 31 * result + (possibleGoods != null ? possibleGoods.hashCode() : 0);
        result = 31 * result + (pet != null ? pet.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (notes != null ? notes.hashCode() : 0);
        result = 31 * result + (issues ? 1 : 0);
        result = 31 * result + (internetCode != null ? internetCode.hashCode() : 0);
        return result;
    }

    @Override
    public String getDiff(ClientDTO another) {
        StringBuilder sb = new StringBuilder();
        diff(firstName, another.firstName, sb, "Имя");
        diff(secondName, another.secondName, sb, "Отчество");
        diff(surname, another.surname, sb, "Фамилия");
        diff(phone, another.phone, sb, "Телефон");
        diff(email, another.email, sb, "email");
        diff(type.getName(), another.type.getName(), sb, "Тип");
        diff(possibleGoods, another.possibleGoods, sb, "Возм. товары");
        diff(pet, another.pet, sb, "Питомец");
        diff(address, another.address, sb, "Адрес");
        diff(notes, another.notes, sb, "Заметки");
        diff(issues, another.issues, sb, "Проблемы");
        diff(internetCode, another.internetCode, sb, "Интернет код");
        return sb.toString();
    }
}
