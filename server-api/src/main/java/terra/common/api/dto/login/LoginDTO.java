package terra.common.api.dto.login;


import terra.common.api.permissions.AuthType;

import java.io.Serializable;

public class LoginDTO implements Serializable {
    private final String version;
    private final String login;
    private final String pass;
    private final AuthType authType;

    public LoginDTO(String version, String login, String pass, AuthType authType) {
        this.version = version;
        this.login = login;
        this.pass = pass;
        this.authType = authType;
    }

    public String getVersion() {
        return version;
    }

    public String getLogin() {
        return login;
    }

    public String getPass() {
        return pass;
    }

    public AuthType getAuthType() {
        return authType;
    }

    @Override
    public String toString() {
        return "LoginDTO{" +
                "version='" + version + '\'' +
                ", login='" + login + '\'' +
                ", pass='" + pass + '\'' +
                ", authType='" + authType + '\'' +
                '}';
    }
}
