package terra.common.api.dto.reports.orderedgoods;

import terra.common.api.dto.filters.BaseFilter;
import terra.common.utils.TimeUtils;
import terra.common.utils.Utils;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;


public class OrderedGoodsFilter extends BaseFilter {
    private Date since;
    private Date till;
    private long stateId = Utils.UNDEFINED_ID;

    public void setStateId(long stateId) {
        this.stateId = stateId;
    }

    public Date getSince() {
        return since;
    }

    public void setSince(Date since) {
        this.since = TimeUtils.getStartOfDay(since);
    }

    public Date getTill() {
        return till;
    }

    public void setTill(Date till) {
        this.till = TimeUtils.getEndOfDay(till);
    }

    @Override
    public String getFilterString() {
        StringBuilder result = new StringBuilder();
        addDateFilter(result, "Заказы.Дата_доставки", since, true);
        addDateFilter(result, "Заказы.Дата_доставки", till, false);
        if (stateId != Utils.UNDEFINED_ID)
            addLongFilter(result, "Заказы.Код_состояния", stateId);
        return result.toString();
    }

    @Override
    public int setupStatement(PreparedStatement ps, int index) throws SQLException {
        index = setupDatePS(ps, index, since, true);
        index = setupDatePS(ps, index, till, false);
        if (stateId != Utils.UNDEFINED_ID)
            index = setupLongPS(ps, index, stateId);
        return index;
    }

    @Override
    public String toString() {
        return "OrderedGoodsFilter{" +
                "since=" + since +
                ", till=" + till +
                ", stateId=" + stateId +
                '}';
    }
}
