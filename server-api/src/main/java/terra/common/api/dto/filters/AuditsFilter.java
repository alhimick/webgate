package terra.common.api.dto.filters;


import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Arrays;

public class AuditsFilter extends BaseFilter {
    private String[] types;
    private Long code;
    private String action;
    private Timestamp from;
    private Timestamp till;


    public void setTypes(String types[]) {
        this.types = types;
    }

    public void setCode(long code) {
        this.code = code;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public void setFrom(Timestamp from) {
        this.from = from;
    }

    public void setTill(Timestamp till) {
        this.till = till;
    }

    @Override
    public String getFilterString() {
        StringBuilder result = new StringBuilder();
        addArrayInFilter(result, "Type", types);
        addLongFilter(result, "RecordId", code);
        addStringFilter(result, "Action", action);
        addDateFilter(result, "ActionDate", from, true);
        addDateFilter(result, "ActionDate", till, false);
        if (result.toString().isEmpty())
            return " ";
        return result.toString();
    }

    @Override
    public int setupStatement(PreparedStatement ps, int index) throws SQLException {
        if (types != null) {
            for (String type : types) {
                index = setupStringPS(ps, index, type, true);
            }
        }
        index = setupLongPS(ps, index, code);
        index = setupStringPS(ps, index, action, true);
        index = setupDatePS(ps, index, from, true);
        index = setupDatePS(ps, index, till, false);
        return index;
    }

    @Override
    public String toString() {
        return "AuditsFilter{" +
                "types='" + Arrays.toString(types) + '\'' +
                ", code=" + code +
                ", action='" + action + '\'' +
                ", from=" + from +
                ", till=" + till +
                '}';
    }
}
