package terra.common.api.dto;


import terra.common.api.audits.AuditAction;
import terra.common.api.audits.AuditType;

import java.io.Serializable;
import java.sql.Timestamp;

public class AuditDTO implements Serializable, Identifiable {
    private final long id;
    private final Timestamp time;
    private final String user;
    private final long recordId;
    private final AuditType type;
    private final AuditAction action;
    private final String message;

    public AuditDTO(long id, Timestamp time, String user, long recordId, AuditType type, AuditAction action, String message) {
        this.id = id;
        this.time = time;
        this.user = user;
        this.recordId = recordId;
        this.type = type;
        this.action = action;
        this.message = message;
    }

    public long getId() {
        return id;
    }

    public Timestamp getTime() {
        return time;
    }

    public String getUser() {
        return user;
    }

    public long getRecordId() {
        return recordId;
    }

    public AuditType getType() {
        return type;
    }

    public AuditAction getAction() {
        return action;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "AuditDTO{" +
                "id=" + id +
                ", time=" + time +
                ", user='" + user + '\'' +
                ", recordId=" + recordId +
                ", type='" + type + '\'' +
                ", action='" + action + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
