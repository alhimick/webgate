package terra.common.api.dto.filters;


public class FilterUtils {
    private FilterUtils() {
    }

    public static String inClause(int count) {
        StringBuilder sb = new StringBuilder();
        sb.append("in (");
        for (int i = 0; i < count; i++) {
            sb.append("?");
            if (i < count - 1)
                sb.append(", ");
        }
        sb.append(")");
        return sb.toString();
    }
}
