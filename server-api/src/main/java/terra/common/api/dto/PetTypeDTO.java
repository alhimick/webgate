package terra.common.api.dto;


import java.io.Serializable;

public class PetTypeDTO implements Identifiable, Nameful, Serializable {
    private final long id;
    private final String name;

    public PetTypeDTO(long id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public String toString() {
        return "PetTypeDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
