package terra.common.api.dto;


import java.io.Serializable;

public class GlobalPropertyDTO implements Serializable {
    private final String key;
    private final String value;

    public GlobalPropertyDTO(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "GlobalPropertyDTO{" +
                "key='" + key + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
