package terra.common.api.dto.filters;


import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;

public class SalonOrderFilter extends BaseFilter {
    private Long id;
    private Long petId;
    private Timestamp start;
    private Timestamp finish;
    private Long masterId;

    public void setId(Long id) {
        this.id = id;
    }

    public void setPetId(Long petId) {
        this.petId = petId;
    }

    public void setStart(Timestamp start) {
        this.start = start;
    }

    public void setFinish(Timestamp finish) {
        this.finish = finish;
    }

    public void setMasterId(Long masterId) {
        this.masterId = masterId;
    }

    @Override
    public String getFilterString() {
        StringBuilder result = new StringBuilder();
        addLongFilter(result, "id", id);
        addLongFilter(result, "pet_id", petId);
        addDateFilter(result, "order_start_time", start, true);
        addDateFilter(result, "order_end_time", finish, false);
        addLongFilter(result, "master_id", masterId);
        if (result.toString().isEmpty())
            return " ";
        return result.toString();
    }

    @Override
    public int setupStatement(PreparedStatement ps, int index) throws SQLException {
        index = setupLongPS(ps, index, id);
        index = setupLongPS(ps, index, petId);
        index = setupTimestampPS(ps, index, start, true);
        index = setupTimestampPS(ps, index, finish, false);
        index = setupLongPS(ps, index, masterId);
        return index;
    }

    @Override
    public String toString() {
        return "SalonOrderFilter{" +
                "id=" + id +
                ", petId=" + petId +
                ", start=" + start +
                ", finish=" + finish +
                ", masterId=" + masterId +
                "} " + super.toString();
    }
}
