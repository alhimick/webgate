package terra.common.api.dto.filters;


import terra.common.utils.TimeUtils;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public abstract class BaseFilter implements Serializable {
    protected static final String AND = " and ";
    protected static final String OR = " or ";

    public abstract String getFilterString();

    public abstract int setupStatement(PreparedStatement ps, int index) throws SQLException;

    protected void addStringFilter(StringBuilder sb, String field, String value) {
        if (value == null)
            return;
        addKeyword(sb, AND);
        appendLike(sb, field);
    }

    protected int setupStringPS(PreparedStatement ps, int index, String value, boolean strict) throws SQLException {
        if (value == null) {
            return index;
        }
        if (!strict)
            value = "%" + value + "%";
        ps.setString(++index, value);
        return index;
    }

    protected void addMultiStringFilter(StringBuilder sb, List<String> fields, String value) {
        if (value == null)
            return;
        addKeyword(sb, AND);
        sb.append("(");
        Iterator<String> iter = fields.iterator();
        while (iter.hasNext()) {
            String field = iter.next();
            appendLike(sb, field);
            if (iter.hasNext())
                sb.append(OR);
        }
        sb.append(")");
    }

    private void appendLike(StringBuilder sb, String field) {
        sb.append(field).append(" like ? ");
    }

    protected void addLongFilter(StringBuilder sb, String field, Long value) {
        if (value == null)
            return;
        addKeyword(sb, AND).append(" ").append(field).append("= ? ");
    }

    protected int setupLongPS(PreparedStatement ps, int index, Long value) throws SQLException {
        if (value == null) {
            return index;
        }
        ps.setLong(++index, value);
        return index;
    }

    protected void addDateFilter(StringBuilder sb, String field, Date value, boolean more) {
        if (value == null)
            return;
        addKeyword(sb, AND).append(" ").append(field);
        if (more) {
            sb.append(" >= ");
        } else {
            sb.append(" <= ");
        }
        sb.append("? ");
    }

    protected int setupDatePS(PreparedStatement ps, int index, Date value, boolean more) throws SQLException {
        if (value == null) {
            return index;
        }
        Date date = more ? TimeUtils.getStartOfDay(value.getTime()) : TimeUtils.getEndOfDay(value.getTime());
        ps.setDate(++index, new java.sql.Date(date.getTime()));
        return index;
    }

    protected int setupTimestampPS(PreparedStatement ps, int index, Date value, boolean more) throws SQLException {
        if (value == null) {
            return index;
        }
        long time = value.getTime();
        Date date = more ? TimeUtils.getStartOfDay(time) : TimeUtils.getEndOfDay(time);
        ps.setTimestamp(++index, new Timestamp(date.getTime()));
        return index;
    }

    protected void addArrayInFilter(StringBuilder sb, String field, Object[] values) {
        if (values == null || values.length == 0)
            return;
        addKeyword(sb, AND).append(" ").append(field).append(" in (");
        for (int i = 0; i < values.length; i++) {
            sb.append("?");
            if (i < values.length - 1)
                sb.append(", ");
        }
        sb.append(") ");

    }

    protected StringBuilder addKeyword(StringBuilder sb, String keyWord) {
        if (sb.toString().isEmpty()) {
            sb.append(" where ");
        } else {
            sb.append(keyWord);
        }
        return sb;
    }
}
