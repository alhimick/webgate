package terra.common.api.dto;


import java.io.Serializable;

public class SalonDTO implements Identifiable, Serializable {
    private final long id;
    private final String address;

    public SalonDTO(long id, String address) {
        this.id = id;
        this.address = address;
    }

    @Override
    public long getId() {
        return id;
    }

    public String getAddress() {
        return address;
    }

    @Override
    public String toString() {
        return "SalonDTO{" +
                "id=" + id +
                ", address='" + address + '\'' +
                '}';
    }
}
