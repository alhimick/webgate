package terra.common.api.dto;


public interface Nameful {
    String getName();
}
