package terra.common.api.dto.reports;

import javax.swing.table.TableModel;
import java.io.Serializable;
import java.util.Map;


public interface SimpleReport extends Serializable {
    Map<String, Object> getFields();

    TableModel getTableModel();
}
