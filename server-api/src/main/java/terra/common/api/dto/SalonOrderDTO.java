package terra.common.api.dto;

import java.io.Serializable;
import java.sql.Timestamp;


public class SalonOrderDTO implements Identifiable, Serializable {
    private final long id;
    private final long petId;
    private final Timestamp start;
    private final Timestamp finish;
    private final String notes;
    private final double cost;
    private final long masterId;
    private final boolean canceled;
    private SalonPetDTO pet;
    private StaffDTO master;

    public SalonOrderDTO(long id, long petId, Timestamp start, Timestamp finish, String notes, double cost,
                         long masterId, boolean canceled) {
        this.id = id;
        this.petId = petId;
        this.start = start;
        this.finish = finish;
        this.notes = notes;
        this.cost = cost;
        this.masterId = masterId;
        this.canceled = canceled;
    }

    @Override
    public long getId() {
        return id;
    }

    public long getPetId() {
        return petId;
    }

    public Timestamp getStart() {
        return start;
    }

    public Timestamp getFinish() {
        return finish;
    }

    public String getNotes() {
        return notes;
    }

    public double getCost() {
        return cost;
    }

    public long getMasterId() {
        return masterId;
    }

    public boolean isCanceled() {
        return canceled;
    }

    public SalonPetDTO getPet() {
        return pet;
    }

    public void setPet(SalonPetDTO pet) {
        this.pet = pet;
    }

    public StaffDTO getMaster() {
        return master;
    }

    public void setMaster(StaffDTO master) {
        this.master = master;
    }

    @Override
    public String toString() {
        return "SalonOrderDTO{" +
                "id=" + id +
                ", petId=" + petId +
                ", start=" + start +
                ", finish=" + finish +
                ", notes='" + notes + '\'' +
                ", cost=" + cost +
                ", masterId=" + masterId +
                ", canceled=" + canceled +
                ", pet=" + pet +
                ", master=" + master +
                '}';
    }
}
