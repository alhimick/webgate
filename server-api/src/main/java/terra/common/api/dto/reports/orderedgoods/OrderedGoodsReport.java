package terra.common.api.dto.reports.orderedgoods;


import terra.common.api.dto.reports.SimpleReport;
import terra.common.api.dto.reports.TeraTableModel;
import terra.common.utils.TeraMath;

import javax.swing.table.TableModel;
import java.sql.Date;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class OrderedGoodsReport implements SimpleReport {
    private final Map<String, Object> fields = new HashMap<>();
    private final TableModel model;

    public OrderedGoodsReport(List<OrderedGoodsDTO> reportData, Date from, Date till) {
        fields.put("from", from.toString());
        fields.put("till", till.toString());
        List<List<String>> data = reportData.stream().map(item -> Arrays.asList(item.getArticle(), item.getName(),
                Integer.toString(item.getTotalCount()),
                Double.toString(TeraMath.roundToRoubles(item.getTotalCost())) + "р.",
                Double.toString(TeraMath.round(item.getTotalWeight())),
                item.getOrders())
        ).collect(Collectors.toList());
        model = new TeraTableModel(Arrays.asList("article", "name", "totalCount", "totalCost", "totalWeight",
                "orders"), data);
    }

    @Override
    public Map<String, Object> getFields() {
        return fields;
    }

    @Override
    public TableModel getTableModel() {
        return model;
    }
}
