package terra.common.api.dto.filters;


import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

public class OrderFilter extends BaseFilter {
    private static final List<String> NAME_FIELDS = Arrays.asList("clients.first_name", "clients.second_name", "clients.surname");

    private Long orderCode;
    private String internetCode;
    private String clientAddress;
    private String clientName;
    private String phone;
    private Long clientType;
    private Long driver;
    private Long manager;
    private Long orderStatus;
    private Long clientId;
    private Date deliveryFrom;
    private Date deliveryTill;
    private Date orderDateFrom;
    private Date orderDateTill;
    private Object[] ids;
    private String possiblyGoods;
    private String clientPet;

    public void setOrderCode(Long orderCode) {
        this.orderCode = orderCode;
    }

    public void setInternetCode(String internetCode) {
        this.internetCode = internetCode;
    }

    public void setClientAddress(String clientAddress) {
        this.clientAddress = clientAddress;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setClientType(Long clientType) {
        this.clientType = clientType;
    }

    public void setDriver(Long driver) {
        this.driver = driver;
    }

    public void setManager(Long manager) {
        this.manager = manager;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public void setOrderStatus(Long orderStatus) {
        this.orderStatus = orderStatus;
    }

    public void setDeliveryFrom(Date deliveryFrom) {
        this.deliveryFrom = deliveryFrom;
    }

    public void setDeliveryTill(Date deliveryTill) {
        this.deliveryTill = deliveryTill;
    }

    public void setOrderDateFrom(Date orderDateFrom) {
        this.orderDateFrom = orderDateFrom;
    }

    public void setOrderDateTill(Date orderDateTill) {
        this.orderDateTill = orderDateTill;
    }

    public void setIds(List<Long> ids) {
        this.ids = ids.toArray();
    }

    public void setPossiblyGoods(String possiblyGoods) {
        this.possiblyGoods = possiblyGoods;
    }

    public void setClientPet(String clientPet) {
        this.clientPet = clientPet;
    }

    @Override
    public String getFilterString() {
        StringBuilder result = new StringBuilder();
        addLongFilter(result, "Заказы.Код", orderCode);
        addStringFilter(result, "Заказы.Интернет_номер", internetCode);
        addMultiStringFilter(result, Arrays.asList("clients.first_name", "clients.second_name", "clients.surname"),
                clientName);
        addStringFilter(result, "clients.address", clientAddress);
        addStringFilter(result, "clients.phone", phone);
        addLongFilter(result, "clients.type_id", clientType);
        addStringFilter(result, "clients.possibly_goods", possiblyGoods);
        addPetFilter(result, clientPet);
        addLongFilter(result, "Заказы.Код_продавца", manager);
        addLongFilter(result, "Заказы.Код_водителя", driver);
        addLongFilter(result, "Заказы.Код_состояния", orderStatus);
        addLongFilter(result, "Заказы.Код_клиента", clientId);
        addDateFilter(result, "Заказы.Дата_доставки", deliveryFrom, true);
        addDateFilter(result, "Заказы.Дата_доставки", deliveryTill, false);
        addDateFilter(result, "Заказы.Дата", orderDateFrom, true);
        addDateFilter(result, "Заказы.Дата", orderDateTill, false);
        addArrayInFilter(result, "Заказы.Код", ids);
        if (result.toString().isEmpty())
            return " ";
        return result.toString();
    }

    private void addPetFilter(StringBuilder result, String salonPetType) {
        if (salonPetType == null) {
            return;
        }
        addKeyword(result, AND);
        result.append("(exists(select sp.* from salon_pet sp inner join pet_type on sp.type_id = pet_type.id " +
                "where sp.owner_id = clients.id and pet_type.type_name like ?) or " +
                "clients.pet like ?) ");
    }

    @Override
    public int setupStatement(PreparedStatement ps, int index) throws SQLException {
        index = setupLongPS(ps, index, orderCode);
        index = setupStringPS(ps, index, internetCode, true);
        for (int i = 0; i < NAME_FIELDS.size(); i++) {
            index = setupStringPS(ps, index, clientName, false);
        }
        index = setupStringPS(ps, index, clientAddress, false);
        index = setupStringPS(ps, index, phone, false);
        index = setupLongPS(ps, index, clientType);
        index = setupStringPS(ps, index, possiblyGoods, false);
        index = setupStringPS(ps, index, clientPet, false);
        index = setupStringPS(ps, index, clientPet, false);
        index = setupLongPS(ps, index, manager);
        index = setupLongPS(ps, index, driver);
        index = setupLongPS(ps, index, orderStatus);
        index = setupLongPS(ps, index, clientId);
        index = setupDatePS(ps, index, deliveryFrom, true);
        index = setupDatePS(ps, index, deliveryTill, false);
        index = setupDatePS(ps, index, orderDateFrom, true);
        index = setupDatePS(ps, index, orderDateTill, false);
        if (ids != null) {
            for (Object id : ids) {
                index = setupLongPS(ps, index, (Long) id);
            }
        }
        return index;
    }

    @Override
    public String toString() {
        return "OrderFilter{" +
                "orderCode=" + orderCode +
                ", internetCode='" + internetCode + '\'' +
                ", clientAddress='" + clientAddress + '\'' +
                ", clientName='" + clientName + '\'' +
                ", phone='" + phone + '\'' +
                ", clientType=" + clientType +
                ", driver=" + driver +
                ", manager=" + manager +
                ", orderStatus=" + orderStatus +
                ", clientId=" + clientId +
                ", deliveryFrom=" + deliveryFrom +
                ", deliveryTill=" + deliveryTill +
                ", orderDateFrom=" + orderDateFrom +
                ", orderDateTill=" + orderDateTill +
                ", ids=" + Arrays.toString(ids) +
                '}';
    }
}
