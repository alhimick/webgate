package terra.common.api.dto.reports.clientreport;


import terra.common.api.dto.reports.SimpleReport;
import terra.common.api.dto.reports.TeraTableModel;

import javax.swing.table.TableModel;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ClientReport implements SimpleReport {
    private final TableModel model;

    public ClientReport(List<ClientReportItemDTO> items) {
        List<List<String>> data = items.stream().map(item -> Arrays.asList(getOrDefault(item.getSurname()),
                item.getFirstName(), getOrDefault(item.getSecondName()),
                getOrDefault(item.getEmail()), getOrDefault(item.getPhone()))
        ).collect(Collectors.toList());
        model = new TeraTableModel(Arrays.asList("surname", "firstName", "secondName", "email", "phone"), data);
    }

    private String getOrDefault(String element) {
        return element == null ? "" : element;
    }

    @Override
    public Map<String, Object> getFields() {
        return new HashMap<>();
    }

    @Override
    public TableModel getTableModel() {
        return model;
    }
}
