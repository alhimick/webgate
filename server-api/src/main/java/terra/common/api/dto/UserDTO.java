package terra.common.api.dto;


import terra.common.api.permissions.AuthType;
import terra.common.api.permissions.Roles;

import java.io.Serializable;

public class UserDTO implements Serializable {
    private final String login;
    private final Roles role;
    private final AuthType authType;
    private final String password;

    public UserDTO(String login, Roles role, AuthType authType, String password) {
        this.login = login;
        this.role = role;
        this.authType = authType;
        this.password = password == null ? "" : password;
    }

    public String getLogin() {
        return login;
    }

    public Roles getRole() {
        return role;
    }

    public AuthType getAuthType() {
        return authType;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return "UserDTO{" +
                "login='" + login + '\'' +
                ", role=" + role +
                ", authType=" + authType +
                ", password='" + password + '\'' +
                '}';
    }
}
