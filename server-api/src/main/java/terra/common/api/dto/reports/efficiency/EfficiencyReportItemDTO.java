package terra.common.api.dto.reports.efficiency;


import java.io.Serializable;

public class EfficiencyReportItemDTO implements Serializable {
    private final String name;
    private final Double avgBill;
    private final Double totalBill;
    private final Integer amount;

    public EfficiencyReportItemDTO(String name, Double avgBill, Double totalBill, Integer amount) {
        this.name = name;
        this.avgBill = avgBill;
        this.totalBill = totalBill;
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public Double getAvgBill() {
        return avgBill;
    }

    public Double getTotalBill() {
        return totalBill;
    }

    public Integer getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return "EfficiencyReportItemDTO{" +
                "name='" + name + '\'' +
                ", avgBill=" + avgBill +
                ", totalBill=" + totalBill +
                ", amount=" + amount +
                '}';
    }
}
