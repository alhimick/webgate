package terra.common.api.dto;


import java.io.Serializable;

public class OrderStateDTO implements Serializable, Identifiable, Nameful {
    private final long id;
    private final String name;
    private final String internetCode;

    public OrderStateDTO(long id, String name, String internetCode) {
        this.id = id;
        this.name = name;
        this.internetCode = internetCode;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    public String getInternetCode() {
        return internetCode;
    }

    @Override
    public String toString() {
        return "OrderStateDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", internetCode='" + internetCode + '\'' +
                '}';
    }
}
