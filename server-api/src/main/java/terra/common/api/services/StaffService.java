package terra.common.api.services;


import terra.common.api.dto.StaffDTO;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface StaffService extends Remote {
    String MANAGER_LOCK = "ManagerLock";
    String DRIVER_LOCK = "DriverLock";
    String MASTER_LOCK = "MasterLock";

    List<StaffDTO> getStaff(StaffDTO.Type type) throws RemoteException;

    List<StaffDTO> getStaff(StaffDTO.Type type, Boolean isWorking) throws RemoteException;

    void addStaff(StaffDTO.Type type, StaffDTO staff) throws RemoteException;

    void updateStaff(StaffDTO.Type type, StaffDTO staff) throws RemoteException;

    void removeStaff(StaffDTO.Type type, Long staffId) throws RemoteException;
}
