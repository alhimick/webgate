package terra.common.api.services;


import terra.common.api.dto.filters.BaseFilter;
import terra.common.api.dto.reports.SimpleReport;
import terra.common.api.dto.reports.orderedgoods.OrderedGoodsFilter;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.sql.Date;
import java.util.List;

public interface ReportService extends Remote {
    SimpleReport getBill(Long orderId) throws RemoteException;

    List<SimpleReport> getBills(BaseFilter filter) throws RemoteException;

    SimpleReport getRegistry(Long driverId, Long orderStateId, Date deliveryDate) throws RemoteException;

    SimpleReport getEfficiency(Date since, Date till, Long orderStateId) throws RemoteException;

    SimpleReport getOrderedGoods(OrderedGoodsFilter filter) throws RemoteException;

    SimpleReport getClientsContactData(BaseFilter filter) throws RemoteException;

    SimpleReport getSalonHistory(Long salonClientId) throws RemoteException;
}
