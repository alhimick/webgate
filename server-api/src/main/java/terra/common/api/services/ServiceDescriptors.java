package terra.common.api.services;

public enum ServiceDescriptors {
    ORDER_POSITIONS("orderPositionsService"),
    ORDERS("orderService"),
    SESSIONS("sessionService"),
    STAFF("staffService"),
    CLIENTS("clientsService"),
    CLIENT_TYPES("clientTypesService"),
    ORDER_STATES("orderStatesService"),
    AUDITS("auditsService"),
    CONFIGURATION("configuration"),
    REPORTS("reports"),
    SALON_PETS("salonPets"),
    INVOKER("invoker", false);

    private final String descriptor;
    private final boolean serverBean;

    ServiceDescriptors(String descriptor) {
        this(descriptor, true);
    }

    ServiceDescriptors(String descriptor, boolean serverBean) {
        this.descriptor = descriptor;
        this.serverBean = serverBean;
    }

    public String getDescriptor() {
        return descriptor;
    }

    public boolean isServerBean() {
        return serverBean;
    }
}
