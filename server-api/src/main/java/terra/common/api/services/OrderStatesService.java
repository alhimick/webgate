package terra.common.api.services;

import terra.common.api.dto.OrderStateDTO;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;


public interface OrderStatesService extends Remote {
    String LOCK = "OrderStateLock";

    List<OrderStateDTO> getOrderStates() throws RemoteException;

    void addOrderState(OrderStateDTO state) throws RemoteException;

    void updateOrderState(OrderStateDTO state) throws RemoteException;

    void removeOrderState(Long stateId) throws RemoteException;
}
