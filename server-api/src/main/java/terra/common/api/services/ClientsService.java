package terra.common.api.services;


import terra.common.api.dto.ClientDTO;
import terra.common.api.dto.filters.BaseFilter;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface ClientsService extends Remote {
    String LOCK = "ClientLock";

    List<ClientDTO> getClients(BaseFilter filter) throws RemoteException;

    long addClient(ClientDTO client) throws RemoteException;

    void updateClient(ClientDTO client) throws RemoteException;

    void removeClient(Long clientId) throws RemoteException;
}
