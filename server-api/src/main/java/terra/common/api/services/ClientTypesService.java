package terra.common.api.services;


import terra.common.api.dto.ClientTypeDTO;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface ClientTypesService extends Remote {
    String LOCK = "ClientTypeLock";

    List<ClientTypeDTO> getClientTypes() throws RemoteException;

    void addClientType(ClientTypeDTO type) throws RemoteException;

    void updateClientType(ClientTypeDTO type) throws RemoteException;

    void removeClientType(Long clientTypeId) throws RemoteException;
}
