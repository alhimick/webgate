package terra.common.api.services;


import terra.common.api.dto.OrderPositionDTO;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Map;

public interface OrderPositionsService extends Remote {
    List<OrderPositionDTO> getOrderPositions(Long orderId) throws RemoteException;

    Map<Long, List<OrderPositionDTO>> getOrdersPositions(List<Long> orderIds) throws RemoteException;

    void addOrderPositions(List<OrderPositionDTO> positions) throws RemoteException;

    void updateOrderPositions(List<OrderPositionDTO> position, boolean web) throws RemoteException;

    void removeOrderPositions(long orderId) throws RemoteException;
}
