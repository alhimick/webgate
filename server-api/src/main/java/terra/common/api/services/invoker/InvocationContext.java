package terra.common.api.services.invoker;


import java.io.Serializable;

public class InvocationContext implements Serializable {
    private final String login;
    private final String sessionId;

    public InvocationContext(String login, String sessionId) {
        this.login = login;
        this.sessionId = sessionId;
    }

    public String getLogin() {
        return login;
    }

    public String getSessionId() {
        return sessionId;
    }

    @Override
    public String toString() {
        return "InvocationContextImpl{login='" + login + "\', sessionId='" + sessionId + "\'}";
    }
}
