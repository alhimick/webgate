package terra.common.api.services;


import terra.common.api.dto.GlobalPropertyDTO;
import terra.common.api.dto.UserDTO;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface ConfigurationService extends Remote {
    String USER_LOCK = "UserLock";

    GlobalPropertyDTO getProperty(String key) throws RemoteException;

    void saveProperty(String key, String value) throws RemoteException;

    List<UserDTO> getUsers() throws RemoteException;

    void addUser(UserDTO user) throws RemoteException;

    void removeUser(String login) throws RemoteException;
}
