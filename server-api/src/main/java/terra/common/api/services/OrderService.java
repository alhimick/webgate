package terra.common.api.services;


import terra.common.api.dto.OrderDTO;
import terra.common.api.dto.ProductDTO;
import terra.common.api.dto.SalonOrderDTO;
import terra.common.api.dto.filters.BaseFilter;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface OrderService extends Remote {
    String ORDER_LOCK = "OrderLock";
    String SALON_ORDER_LOCK = "SalonOrderLock";

    OrderDTO getOrder(Long orderId) throws RemoteException;

    OrderDTO getOrderWithPositions(Long orderId) throws RemoteException;

    List<OrderDTO> getOrders(BaseFilter filter) throws RemoteException;

    long addOrder(OrderDTO order) throws RemoteException;

    int updateOrders(List<OrderDTO> orders) throws RemoteException;

    void removeOrder(Long orderId) throws RemoteException;

    List<Long> changeOrdersState(BaseFilter filter, Long newStateId) throws RemoteException;

    long countOrders(BaseFilter filter) throws RemoteException;

    void sync(List<OrderDTO> orders, String webStatus, String manager, String driver, String type) throws RemoteException;

    List<ProductDTO.Ext> getProducts(String namePart) throws RemoteException;

    void updateProduct(Long id) throws RemoteException;

    List<SalonOrderDTO> getSalonOrders(BaseFilter filter) throws RemoteException;

    long addSalonOrder(SalonOrderDTO order) throws RemoteException;

    void updateSalonOrder(SalonOrderDTO order) throws RemoteException;

    void removeSalonOrder(Long orderId) throws RemoteException;
}
