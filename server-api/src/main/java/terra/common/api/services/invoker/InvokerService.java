package terra.common.api.services.invoker;


import terra.common.api.methods.MethodDescriptor;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface InvokerService extends Remote {
    Object invoke(InvocationContext ctx, MethodDescriptor md, Object[] arguments) throws RemoteException;
}
