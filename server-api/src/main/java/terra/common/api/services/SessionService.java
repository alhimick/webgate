package terra.common.api.services;

import terra.common.api.dto.login.LoginDTO;
import terra.common.api.dto.login.LoginReplyDTO;

import java.rmi.Remote;
import java.rmi.RemoteException;


public interface SessionService extends Remote {
    LoginReplyDTO login(LoginDTO request) throws RemoteException;

    long ping() throws RemoteException;

    Boolean lock(String descriptor, Long id) throws RemoteException;

    Boolean isLockedForUser(String descriptor, Long id) throws RemoteException;

    Boolean unlock(String descriptor, Long id) throws RemoteException;

    Boolean isValidKey(String key) throws RemoteException;
}
