package terra.common.api.services;


import terra.common.api.dto.AuditDTO;
import terra.common.api.dto.filters.BaseFilter;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface AuditsService extends Remote {
    List<AuditDTO> getAudits(BaseFilter filter) throws RemoteException;
}
