package terra.common.api.services;


import terra.common.api.dto.PetTypeDTO;
import terra.common.api.dto.SalonPetDTO;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface SalonPetService extends Remote {
    String LOCK = "PetLock";
    String TYPE_LOCK = "TypeLock";

    List<SalonPetDTO> getSalonPets(Long ownerId) throws RemoteException;

    SalonPetDTO getSalonPet(Long petId) throws RemoteException;

    void addSalonPet(SalonPetDTO pet) throws RemoteException;

    void updateSalonPet(SalonPetDTO pet) throws RemoteException;

    void removeSalonPet(Long petId) throws RemoteException;

    void addType(PetTypeDTO type) throws RemoteException;

    void removeType(Long id) throws RemoteException;

    List<PetTypeDTO> getTypes() throws RemoteException;
}
