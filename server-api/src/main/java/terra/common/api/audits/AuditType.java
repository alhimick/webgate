package terra.common.api.audits;


public enum AuditType {
    ORDER("Заказ"), ORDER_POSITION("Позиция"), CLIENT("Клиент"), UNKNOWN("Неизвестно");

    private final String displayName;

    AuditType(String displayName) {
        this.displayName = displayName;
    }

    public static AuditType lookup(String displayName) {
        for (AuditType auditType : values()) {
            if (auditType.displayName.equals(displayName))
                return auditType;
        }
        return UNKNOWN;
    }

    public static AuditType getByName(String string) {
        try {
            return valueOf(string);
        } catch (Exception e) {
            return UNKNOWN;
        }
    }

    public String getDisplayName() {
        return displayName;
    }
}
