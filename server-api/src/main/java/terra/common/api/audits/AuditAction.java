package terra.common.api.audits;


public enum AuditAction {
    ADD("Добавление"), UPDATE("Обновление"), REMOVE("Удаление"), UNKNOWN("Неизвестно");

    private final String displayName;

    AuditAction(String displayName) {
        this.displayName = displayName;
    }

    public static AuditAction lookup(String displayName) {
        for (AuditAction auditAction : values()) {
            if (auditAction.displayName.equals(displayName))
                return auditAction;
        }
        return UNKNOWN;
    }

    public static AuditAction getByName(String string) {
        try {
            return valueOf(string);
        } catch (Exception e) {
            return UNKNOWN;
        }
    }

    public String getDisplayName() {
        return displayName;
    }
}
