package terra.common.api.permissions;


import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static terra.common.api.permissions.Permission.*;

public enum Roles {
    ADMIN("Админ", Permission.values()),
    MANAGER("Продавец", new Permission[]{EDIT_CLIENTS, ADD_ORDER, EDIT_ORDER, GOODS_ORDERS, COMMONS_MENU}),
    SENIOR_MANAGER("Ст. продавец", new Permission[]{EDIT_CLIENTS, ADD_ORDER, EDIT_ORDER, EFFICIENCY_REPORT,
            GOODS_ORDERS, COMMONS_MENU, DELIVERY_REPORT}),
    LOGIST("Логист", new Permission[]{GOODS_ORDERS, COMMONS_MENU}),
    GOODS_KEEPER("Товаровед", new Permission[]{EDIT_ORDER, GOODS_KEEPER_MENU, GOODS_ORDERS, COMMONS_MENU}),
    SALON_OPERATOR("Администратор салона", new Permission[]{SALON_MENU, EDIT_CLIENTS}),
    NO_ACCESS("Нет доступа", new Permission[]{}),
    INCOMPATIBLE("Несовместимые версии", new Permission[]{}, false);

    private final String displayName;
    private final List<Permission> permissions;
    private final boolean assignable;

    Roles(String displayName, Permission[] permissions) {
        this(displayName, permissions, true);
    }

    Roles(String displayName, Permission[] permissions, boolean assignable) {
        this.displayName = displayName;
        this.permissions = Arrays.asList(permissions);
        this.assignable = assignable;
    }

    public String getDisplayName() {
        return displayName;
    }

    public List<Permission> getPermissions() {
        return Collections.unmodifiableList(permissions);
    }

    public boolean isAssignable() {
        return assignable;
    }
}
