package terra.common.api.permissions;


public enum Permission {
    REMOVAL, // removal button on all frames
    DIRECTOR_MENU,
    GOODS_KEEPER_MENU,
    SALON_MENU,
    EDIT_CLIENTS,
    GOODS_ORDERS,
    ADD_ORDER,
    EDIT_ORDER,
    EFFICIENCY_REPORT,
    DELIVERY_REPORT,
    COMMONS_MENU,
}
