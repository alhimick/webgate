package terra.common.api.permissions;


import terra.common.utils.AuthUtils;

public enum AuthType {
    HOST("Компьютер") {
        @Override
        public String getLogin() {
            return AuthUtils.getComputerName();
        }
    },
    LOGIN("Логин") {
        @Override
        public String getLogin() {
            return AuthUtils.getLogin();
        }

        @Override
        public boolean needPassword() {
            return true;
        }
    },
    USER("Пользователь") {
        @Override
        public String getLogin() {
            return AuthUtils.getUser();
        }
    };

    private final String name;

    AuthType(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return name;
    }

    public abstract String getLogin();

    public boolean needPassword() {
        return false;
    }
}
