package terra.common.api.methods;


import terra.common.api.dto.OrderDTO;
import terra.common.api.dto.SalonOrderDTO;
import terra.common.api.dto.filters.BaseFilter;
import terra.common.api.services.ServiceDescriptors;

import java.util.List;

public class OrdersDescriptor extends MethodDescriptor {
    public static final OrdersDescriptor GET_ORDER = new OrdersDescriptor("getOrder", Long.class);
    public static final OrdersDescriptor GET_ORDER_WITH_POSITIONS = new OrdersDescriptor("getOrderWithPositions", Long.class);

    public static final OrdersDescriptor GET_ORDERS = new OrdersDescriptor("getOrders", BaseFilter.class);
    public static final OrdersDescriptor ADD_ORDER = new OrdersDescriptor("addOrder", OrderDTO.class);
    public static final OrdersDescriptor UPDATE_ORDERS = new OrdersDescriptor("updateOrders", List.class);
    public static final OrdersDescriptor REMOVE_ORDER = new OrdersDescriptor("removeOrder", Long.class);
    public static final OrdersDescriptor MOVE_ORDERS = new OrdersDescriptor("changeOrdersState", BaseFilter.class, Long.class);
    public static final OrdersDescriptor COUNT_ORDERS = new OrdersDescriptor("countOrders", BaseFilter.class);
    public static final OrdersDescriptor SYNC_ORDERS = new OrdersDescriptor("sync", List.class, String.class, String.class, String.class, String.class);
    public static final OrdersDescriptor GET_PRODUCTS = new OrdersDescriptor("getProducts", String.class);
    public static final OrdersDescriptor UPDATE_PRODUCTS = new OrdersDescriptor("updateProduct", Long.class);

    public static final OrdersDescriptor GET_SALON_ORDERS = new OrdersDescriptor("getSalonOrders", BaseFilter.class);
    public static final OrdersDescriptor ADD_SALON_ORDER = new OrdersDescriptor("addSalonOrder", SalonOrderDTO.class);
    public static final OrdersDescriptor UPDATE_SALON_ORDER = new OrdersDescriptor("updateSalonOrder", SalonOrderDTO.class);
    public static final OrdersDescriptor REMOVE_SALON_ORDER = new OrdersDescriptor("removeSalonOrder", Long.class);

    public OrdersDescriptor(String method, Class<?>... parameterTypes) {
        super(method, ServiceDescriptors.ORDERS, parameterTypes);
    }
}
