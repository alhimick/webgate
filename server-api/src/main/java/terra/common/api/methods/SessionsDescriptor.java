package terra.common.api.methods;


import terra.common.api.dto.login.LoginDTO;
import terra.common.api.services.ServiceDescriptors;

public class SessionsDescriptor extends MethodDescriptor {
    public static final SessionsDescriptor LOGIN = new SessionsDescriptor("login", LoginDTO.class);
    public static final SessionsDescriptor PING = new SessionsDescriptor("ping");
    public static final SessionsDescriptor LOCK = new SessionsDescriptor("lock", String.class, Long.class);
    public static final SessionsDescriptor UNLOCK = new SessionsDescriptor("unlock", String.class, Long.class);

    public SessionsDescriptor(String method, Class<?>... parameterTypes) {
        super(method, ServiceDescriptors.SESSIONS, parameterTypes);
    }
}
