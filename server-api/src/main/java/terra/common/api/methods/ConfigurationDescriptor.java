package terra.common.api.methods;


import terra.common.api.dto.UserDTO;
import terra.common.api.services.ServiceDescriptors;

public class ConfigurationDescriptor extends MethodDescriptor {
    public static final ConfigurationDescriptor GET_PROPERTY = new ConfigurationDescriptor("getProperty", String.class);
    public static final ConfigurationDescriptor SET_PROPERTY = new ConfigurationDescriptor("saveProperty", String.class, String.class);

    public static final ConfigurationDescriptor GET_USERS = new ConfigurationDescriptor("getUsers");
    public static final ConfigurationDescriptor ADD_USER = new ConfigurationDescriptor("addUser", UserDTO.class);
    public static final ConfigurationDescriptor REMOVE_USER = new ConfigurationDescriptor("removeUser", String.class);

    public ConfigurationDescriptor(String method, Class<?>... parameterTypes) {
        super(method, ServiceDescriptors.CONFIGURATION, parameterTypes);
    }
}
