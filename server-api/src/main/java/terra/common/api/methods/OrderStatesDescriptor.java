package terra.common.api.methods;


import terra.common.api.dto.OrderStateDTO;
import terra.common.api.services.ServiceDescriptors;

public class OrderStatesDescriptor extends MethodDescriptor {
    public static final OrderStatesDescriptor GET_ORDER_STATES = new OrderStatesDescriptor("getOrderStates");
    public static final OrderStatesDescriptor ADD_ORDER_STATE = new OrderStatesDescriptor("addOrderState", OrderStateDTO.class);
    public static final OrderStatesDescriptor UPDATE_ORDER_STATE = new OrderStatesDescriptor("updateOrderState", OrderStateDTO.class);
    public static final OrderStatesDescriptor REMOVE_ORDER_STATE = new OrderStatesDescriptor("removeOrderState", Long.class);


    public OrderStatesDescriptor(String method, Class<?>... parameterTypes) {
        super(method, ServiceDescriptors.ORDER_STATES, parameterTypes);
    }
}
