package terra.common.api.methods;


import terra.common.api.dto.PetTypeDTO;
import terra.common.api.dto.SalonPetDTO;
import terra.common.api.services.ServiceDescriptors;

public class SalonPetsDescriptor extends MethodDescriptor {
    public static final SalonPetsDescriptor GET_PETS = new SalonPetsDescriptor("getSalonPets", Long.class);
    public static final SalonPetsDescriptor GET_PET = new SalonPetsDescriptor("getSalonPet", Long.class);
    public static final SalonPetsDescriptor ADD_PET = new SalonPetsDescriptor("addSalonPet", SalonPetDTO.class);
    public static final SalonPetsDescriptor UPDATE_PET = new SalonPetsDescriptor("updateSalonPet", SalonPetDTO.class);
    public static final SalonPetsDescriptor REMOVE_PET = new SalonPetsDescriptor("removeSalonPet", Long.class);

    public static final SalonPetsDescriptor GET_TYPES = new SalonPetsDescriptor("getTypes");
    public static final SalonPetsDescriptor ADD_TYPE = new SalonPetsDescriptor("addType", PetTypeDTO.class);
    public static final SalonPetsDescriptor REMOVE_TYPE = new SalonPetsDescriptor("removeType", Long.class);

    public SalonPetsDescriptor(String method, Class<?>... parameterTypes) {
        super(method, ServiceDescriptors.SALON_PETS, parameterTypes);
    }
}
