package terra.common.api.methods;


import terra.common.api.services.ServiceDescriptors;

import java.io.Serializable;
import java.util.Arrays;

public abstract class MethodDescriptor implements Serializable {
    private final String method;
    private final ServiceDescriptors descriptor;
    private final Class<?>[] parameterTypes;

    public MethodDescriptor(String method, ServiceDescriptors descriptor, Class<?>[] parameterTypes) {
        this.method = method;
        this.descriptor = descriptor;
        this.parameterTypes = parameterTypes;
    }

    public String getMethod() {
        return method;
    }

    public ServiceDescriptors getDescriptor() {
        return descriptor;
    }

    public Class<?>[] getParameterTypes() {
        return parameterTypes;
    }

    @Override
    public String toString() {
        return "MethodDescriptor{" +
                "method='" + method + '\'' +
                ", descriptor=" + descriptor +
                ", parameterTypes=" + Arrays.toString(parameterTypes) +
                '}';
    }
}
