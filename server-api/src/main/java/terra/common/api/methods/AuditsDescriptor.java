package terra.common.api.methods;


import terra.common.api.dto.filters.BaseFilter;
import terra.common.api.services.ServiceDescriptors;

public class AuditsDescriptor extends MethodDescriptor {
    public static final AuditsDescriptor GET_AUDITS = new AuditsDescriptor("getAudits", BaseFilter.class);

    public AuditsDescriptor(String method, Class<?>... parameterTypes) {
        super(method, ServiceDescriptors.AUDITS, parameterTypes);
    }
}
