package terra.common.api.methods;


import terra.common.api.dto.StaffDTO;
import terra.common.api.services.ServiceDescriptors;

public class StaffDescriptor extends MethodDescriptor {
    public static final StaffDescriptor GET_STAFF = new StaffDescriptor("getStaff", StaffDTO.Type.class);
    public static final StaffDescriptor GET_STAFF_WORKING = new StaffDescriptor("getStaff", StaffDTO.Type.class, Boolean.class);
    public static final StaffDescriptor ADD_STAFF = new StaffDescriptor("addStaff", StaffDTO.Type.class, StaffDTO.class);
    public static final StaffDescriptor UPDATE_STAFF = new StaffDescriptor("updateStaff", StaffDTO.Type.class, StaffDTO.class);
    public static final StaffDescriptor REMOVE_STAFF = new StaffDescriptor("removeStaff", StaffDTO.Type.class, Long.class);

    public StaffDescriptor(String method, Class<?>... parameterTypes) {
        super(method, ServiceDescriptors.STAFF, parameterTypes);
    }
}
