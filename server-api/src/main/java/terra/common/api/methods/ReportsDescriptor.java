package terra.common.api.methods;

import terra.common.api.dto.filters.BaseFilter;
import terra.common.api.dto.reports.orderedgoods.OrderedGoodsFilter;
import terra.common.api.services.ServiceDescriptors;

import java.sql.Date;


public class ReportsDescriptor extends MethodDescriptor {
    public static final ReportsDescriptor BILL = new ReportsDescriptor("getBill", Long.class);
    public static final ReportsDescriptor BILLS = new ReportsDescriptor("getBills", BaseFilter.class);
    public static final ReportsDescriptor REGISTRY = new ReportsDescriptor("getRegistry", Long.class, Long.class, Date.class);
    public static final ReportsDescriptor EFFICIENCY = new ReportsDescriptor("getEfficiency", Date.class, Date.class, Long.class);
    public static final ReportsDescriptor ORDERED_GOODS = new ReportsDescriptor("getOrderedGoods", OrderedGoodsFilter.class);
    public static final ReportsDescriptor CLIENTS = new ReportsDescriptor("getClientsContactData", BaseFilter.class);
    public static final ReportsDescriptor SALON_HISTORY = new ReportsDescriptor("getSalonHistory", Long.class);

    public ReportsDescriptor(String method, Class... parameterTypes) {
        super(method, ServiceDescriptors.REPORTS, parameterTypes);
    }
}
