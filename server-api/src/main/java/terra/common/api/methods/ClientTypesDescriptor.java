package terra.common.api.methods;


import terra.common.api.dto.ClientTypeDTO;
import terra.common.api.services.ServiceDescriptors;

public class ClientTypesDescriptor extends MethodDescriptor {
    public static final ClientTypesDescriptor GET_TYPES = new ClientTypesDescriptor("getClientTypes");
    public static final ClientTypesDescriptor ADD_TYPE = new ClientTypesDescriptor("addClientType", ClientTypeDTO.class);
    public static final ClientTypesDescriptor UPDATE_TYPE = new ClientTypesDescriptor("updateClientType", ClientTypeDTO.class);
    public static final ClientTypesDescriptor REMOVE_TYPE = new ClientTypesDescriptor("removeClientType", Long.class);


    public ClientTypesDescriptor(String method, Class<?>... parameterTypes) {
        super(method, ServiceDescriptors.CLIENT_TYPES, parameterTypes);
    }
}
