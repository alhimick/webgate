package terra.common.api.methods;


import terra.common.api.dto.ClientDTO;
import terra.common.api.dto.filters.BaseFilter;
import terra.common.api.services.ServiceDescriptors;

public class ClientsDescriptor extends MethodDescriptor {
    public static final ClientsDescriptor GET_CLIENTS = new ClientsDescriptor("getClients", BaseFilter.class);
    public static final ClientsDescriptor ADD_CLIENT = new ClientsDescriptor("addClient", ClientDTO.class);
    public static final ClientsDescriptor UPDATE_CLIENT = new ClientsDescriptor("updateClient", ClientDTO.class);
    public static final ClientsDescriptor REMOVE_CLIENT = new ClientsDescriptor("removeClient", Long.class);

    public ClientsDescriptor(String method, Class<?>... parameterTypes) {
        super(method, ServiceDescriptors.CLIENTS, parameterTypes);
    }
}
