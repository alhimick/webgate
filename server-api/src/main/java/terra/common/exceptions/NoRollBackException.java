package terra.common.exceptions;

public class NoRollBackException extends TeraException {
    public NoRollBackException() {
    }

    public NoRollBackException(String message) {
        super(message);
    }

    public NoRollBackException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoRollBackException(Throwable cause) {
        super(cause);
    }
}
