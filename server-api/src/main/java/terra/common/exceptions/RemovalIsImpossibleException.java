package terra.common.exceptions;


public class RemovalIsImpossibleException extends TeraException {
    public RemovalIsImpossibleException(String cause) {
        super(cause);
    }

    public RemovalIsImpossibleException() {
        super();
    }
}
