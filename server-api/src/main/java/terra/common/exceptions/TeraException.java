package terra.common.exceptions;

/**
 * Base class for all api exceptions
 */
public class TeraException extends RuntimeException {
    public TeraException() {
    }

    public TeraException(String message) {
        super(message);
    }

    public TeraException(String message, Throwable cause) {
        super(message, cause);
    }

    public TeraException(Throwable cause) {
        super(cause);
    }
}
