package terra.common.exceptions;


public class UserAlreadyExistsException extends TeraException {
    public UserAlreadyExistsException(String message) {
        super(message);
    }
}
