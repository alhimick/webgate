package terra.common.utils;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

final public class CollectionUtils {

    public static <E> List<E> asList(Collection<E> c) {
        return c.stream().collect(Collectors.toList());
    }

    public static <K, V> K findKey(Map<K, V> map, V value) {
        for (Map.Entry<K, V> entry : map.entrySet()) {
            if (entry.getValue().equals(value))
                return entry.getKey();
        }
        return null;
    }

    public static <T> List<T> substract(List<T> first, List<T> second) {
        List<T> result = new ArrayList<>(first);
        result.removeAll(second);
        return result;
    }

    public static <T> List<T> common(List<T> first, List<T> second) {
        List<T> tmp = new ArrayList<>(first);
        tmp.removeAll(second);
        List<T> result = new ArrayList<>(first);
        result.removeAll(tmp);
        return result;
    }
}
