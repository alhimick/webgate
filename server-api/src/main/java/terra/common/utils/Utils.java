package terra.common.utils;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import terra.common.exceptions.TeraException;

import java.lang.reflect.InvocationTargetException;
import java.net.ConnectException;
import java.net.SocketException;
import java.rmi.NotBoundException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;

final public class Utils {
    public static final long UNDEFINED_ID = -1;
    public static final Predicate<Throwable> NON_FATAL = e -> e instanceof ConnectException ||
            e instanceof java.rmi.ConnectException || e instanceof NotBoundException ||
            e instanceof SocketException || e instanceof TeraException;
    private static final Logger log = Logger.getLogger(Utils.class);

    private Utils() {
    }

    public static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException ie) {
            Thread.currentThread().interrupt();
        }
    }

    public static void await(CountDownLatch latch) {
        try {
            latch.await();
        } catch (InterruptedException ie) {
            Thread.currentThread().interrupt();
        }
    }

    public static void await(CountDownLatch latch, long time) {
        try {
            latch.await(time, TimeUnit.MILLISECONDS);
        } catch (InterruptedException ie) {
            Thread.currentThread().interrupt();
        }
    }

    public static void close(AutoCloseable closeable) {
        if (closeable == null)
            return;
        try {
            closeable.close();
        } catch (Exception e) {
            log.warn(e, e);
        }
    }

    public static Level getLevel(Throwable t) {
        Level level = Level.ERROR;
        if (t != null && checkCause(t, NON_FATAL)) {
            level = Level.WARN;
        }
        return level;
    }

    public static boolean checkCause(Throwable t, Predicate<Throwable> check) {
        Throwable tmp = t;
        while (tmp != null) {
            if (check.test(tmp))
                return true;
            tmp = tmp.getCause();
        }
        return false;
    }

    public static Throwable unwrap(Throwable t) {
        Throwable tmp = t;
        while (tmp != null) {
            if (!(tmp.getClass().equals(RuntimeException.class) || tmp instanceof ExecutionException ||
                    tmp instanceof InvocationTargetException))
                return tmp;
            tmp = tmp.getCause();
        }
        return null;
    }

    public static void setDefaultErrorsHandler() {
        Thread.setDefaultUncaughtExceptionHandler((t, e) ->
                log.log(getLevel(e), "Unexpected error in [" + t.getName() + "] thread", e));
    }

    public static RuntimeException logFail(Throwable e) {
        log.error(e, e);
        return new RuntimeException(e);
    }
}
