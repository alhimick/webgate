package terra.common.utils;


import java.sql.Date;
import java.sql.Timestamp;
import java.util.Calendar;

public class TimeUtils {
    public static final long MIN = 60 * 1000L;
    public static final long HOUR = 60 * MIN;
    public static final long DAY = 24 * HOUR;

    private TimeUtils() {
    }

    public static Date getStartOfDay(Date date) {
        return date == null ? null : getStartOfDay(date.getTime());
    }

    public static Date getStartOfDay(Timestamp date) {
        return date == null ? null : getStartOfDay(date.getTime());
    }

    public static Date getStartOfDay(long date) {
        return getDateInternal(date, 0, 0, 0);

    }

    public static Date getEndOfDay(Date date) {
        return date == null ? null : getEndOfDay(date.getTime());
    }

    public static Date getEndOfDay(Timestamp date) {
        return date == null ? null : getEndOfDay(date.getTime());
    }

    public static Date getEndOfDay(long date) {
        return getDateInternal(date, 23, 59, 59);
    }

    public static boolean contains(Timestamp time, Timestamp start, Timestamp finish) {
        return start.before(time) && finish.after(time);
    }

    private static Date getDateInternal(long date, int hours, int mins, int seconds) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(date);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DATE);
        calendar.set(year, month, day, hours, mins, seconds);
        return new Date(calendar.getTimeInMillis());
    }
}
