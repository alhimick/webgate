package terra.common.utils;


import org.apache.log4j.Logger;

import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Objects;

public class AuthUtils {
    public static final String TERRA_LOGIN_PROPERTY = "terra.login";
    private static final Logger log = Logger.getLogger(AuthUtils.class);
    private static final String VERSION_DELIMITER = "\\.";

    private AuthUtils() {
    }

    public static String getVersion() {
        String version = System.getProperty("tera.version");
        if (version != null)
            return version;
        try {
            version = Files.readAllLines(new File("version").toPath()).get(0);
            System.setProperty("tera.version", version);
            log.info("----------VERSION: " + version + "----------");
            return version;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static boolean checkVersion(String version) {
        String[] current = getVersion().split(VERSION_DELIMITER);
        String[] other = version.split(VERSION_DELIMITER);

        return !(other.length < 2 || current.length < 2) && (Objects.equals(current[0], other[0]) &&
                Objects.equals(current[1], other[1]));

    }

    public static String getComputerName() {
        try {
            return InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException uhe) {
            log.error(uhe, uhe);
        }
        return null;
    }

    public static String getUser() {
        return System.getenv("username");
    }

    public static String getLogin() {
        return System.getProperty(TERRA_LOGIN_PROPERTY);
    }

    public static String sha256(String pass) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(pass.getBytes(StandardCharsets.UTF_8));
            return Base64.getEncoder().encodeToString(hash);
        } catch (NoSuchAlgorithmException nsae) {
            throw Utils.logFail(nsae);
        }
    }
}
