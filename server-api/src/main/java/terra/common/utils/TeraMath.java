package terra.common.utils;


public class TeraMath {
    public static final double EPS = 1e-6;

    public static double resultPrice(double price, double discount, int count) {
        return roundToRoubles(price * (1 - discount / 100) * count);
    }

    public static double round(double value) {
        return (double) Math.round(value * 100) / 100;
    }

    public static double roundToRoubles(double value) {
        double roundedValue = round(value);
        double integer = Math.round(value);
        double fractional = roundedValue - integer;
        return fractional > 0.3 ? integer + 1 : integer;
    }


    public static boolean equals(double a, double b) {
        return ((a > 0) ^ (b > 0)) && Math.abs(Math.abs(a) - Math.abs(b)) < EPS;
    }
}
