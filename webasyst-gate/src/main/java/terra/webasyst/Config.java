package terra.webasyst;


import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class Config {
    private static final Config instance = new Config();
    private final Properties storage = new Properties();

    private Config() {
        try {
            storage.load(new FileReader("gate.conf"));
        } catch (IOException ioe) {
            throw new RuntimeException(ioe);
        }
    }

    public static Config getInstance() {
        return instance;
    }

    public String getWebasystHost() {
        return storage.getProperty("host");
    }

    public String getApiKey() {
        return storage.getProperty("key");
    }

    int getPort() {
        return Integer.parseInt(storage.getProperty("port", "2012"));
    }

    public String getIdPrefix() {
        return storage.getProperty("prefix", "100");
    }

    public String getEmptyAddress() {
        return storage.getProperty("empty_address", "");
    }

    public boolean isProd() {
        return Boolean.parseBoolean(storage.getProperty("prod", "false"));
    }

    public String getAuth() {
        return storage.getProperty("auth");
    }
}
