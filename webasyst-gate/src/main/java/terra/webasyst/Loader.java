package terra.webasyst;


import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import terra.common.utils.Utils;

import java.rmi.AlreadyBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Map;

public class Loader {
    private static final Logger log = Logger.getLogger(Loader.class);
    private static final String mainFile = "templates/spring-context.xml";
    private static volatile Registry registry;
    private final Map<String, Remote> serviceHolder = new HashMap<>();

    public static void main(String[] args) throws InterruptedException {
        Utils.setDefaultErrorsHandler();
        PropertyConfigurator.configure("log4j.properties");
        new Loader().go();
    }

    private void go() throws InterruptedException {
        log.info("----------STARTING!----------");
        ApplicationContext context = new FileSystemXmlApplicationContext(mainFile);
        setupRegistry();
        try {
            Remote service = (Remote) context.getBean("webasyst");
            serviceHolder.put("webasyst", service);
            Remote stub = UnicastRemoteObject.exportObject(service, Config.getInstance().getPort() +
                    serviceHolder.size() + 1);
            registry.bind("webasyst", stub);
            log.info("webasyst" + ": " + service);
        } catch (RemoteException | AlreadyBoundException e) {
            throw Utils.logFail(e);
        }
        Thread.currentThread().join();
    }

    private void setupRegistry() {
        try {
            registry = LocateRegistry.createRegistry(Config.getInstance().getPort());
        } catch (RemoteException e) {
            throw Utils.logFail(e);
        }
    }
}