package terra.webasyst.server.requests.products;


import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import terra.common.api.dto.ProductDTO;
import terra.webasyst.server.requests.AbstractRequest;

import java.util.ArrayList;
import java.util.List;

public class SkuRequest extends AbstractRequest {

    SkuRequest(RestTemplate template, String baseUrl) {
        super(template, baseUrl);
    }

    public static double parseWeight(String name) {
        try {
            String[] tokens = name.split(" ");
            Weights weight = Weights.findByCaption(tokens[1]);
            return weight.toKg(Double.parseDouble(tokens[0].trim()));
        } catch (NumberFormatException | ArrayIndexOutOfBoundsException ignore) {
            return 0;
        }
    }

    public List<ProductDTO.Ext> getSkuList(long productId, String productName) {
        UriComponentsBuilder builder = UriComponentsBuilder
                .fromHttpUrl(baseUrl + "shop.product.skus.getList")
                .queryParam("access_token", getToken())
                .queryParam("product_id", productId);
        UriComponents productUri = builder.build().encode();
        String skuLine = template.getForObject(productUri.toUri(), String.class);
        JsonArray skuObject = new JsonParser().parse(skuLine).getAsJsonArray();
        List<ProductDTO.Ext> result = new ArrayList<>();
        skuObject.forEach(e -> {
            JsonObject obj = e.getAsJsonObject();
            String name = format(obj.get("name").getAsString(), productName);
            double weight = parseWeight(name);
            result.add(new ProductDTO.Ext(obj.get("id").getAsLong(), productId, name, obj.get("price").getAsDouble(),
                    weight, obj.get("sku").getAsString(), "")); //no code
        });
        return result;
    }

    private String format(String variantName, String productName) {
        return productName + " (" + variantName + ")";
    }

    private enum Weights {
        GRAMM("гр", 0.001), KILOGRAMM("кг", 1);

        private final String caption;
        private final double multiplier;

        Weights(String caption, double multiplier) {
            this.caption = caption;
            this.multiplier = multiplier;
        }

        public static Weights findByCaption(String caption) {
            for (Weights weights : values()) {
                if (weights.caption.equals(caption.trim()))
                    return weights;
            }
            return Weights.KILOGRAMM;
        }

        public double toKg(double weight) {
            return weight * multiplier;
        }
    }
}
