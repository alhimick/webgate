package terra.webasyst.server.requests.orders;


import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import terra.common.api.dto.ClientDTO;
import terra.common.api.dto.OrderDTO;
import terra.common.api.dto.OrderPositionDTO;
import terra.common.api.dto.StaffDTO;
import terra.common.articles.ArticlesData;
import terra.common.articles.ArticlesEncoder;
import terra.common.dtedit.DeliveryTimeModel;
import terra.common.utils.Utils;
import terra.webasyst.Config;
import terra.webasyst.api.utils.MetadataEncoder;
import terra.webasyst.server.requests.AbstractRequest;
import terra.webasyst.server.requests.products.SkuRequest;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

class OrderDetailsRequest extends AbstractRequest {
    private static final Pattern WEIGHT_PARSER = Pattern.compile("^.*\\((.*)\\)$");

    OrderDetailsRequest(RestTemplate template, String baseUrl) {
        super(template, baseUrl);
    }

    OrderDTO getOrderDetails(long id) throws ParseException {
        UriComponentsBuilder uriBuilder = UriComponentsBuilder
                .fromHttpUrl(baseUrl + "shop.order.getInfo")
                .queryParam("access_token", getToken())
                .queryParam("id", id);
        UriComponents uri = uriBuilder.build().encode();
        String line = template.getForObject(uri.toUri(), String.class);
        JsonObject jsonObject = new JsonParser().parse(line).getAsJsonObject();
        String internetCode = Config.getInstance().getIdPrefix() + jsonObject.get("id").getAsString();
        String creation = jsonObject.get("create_datetime").getAsString();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date creationDate = new Date(sdf.parse(creation).getTime());
        JsonObject params = jsonObject.get("params").getAsJsonObject();

        String address = params.has("shipping_address.street") ?
                params.get("shipping_address.street").getAsString() :
                Config.getInstance().getEmptyAddress();
        Date deliveryTime = getDeliveryDate(jsonObject, sdf);
        String since = getDesiredTimeBound(params, "shipping_end_datetime");
        String till = getDesiredTimeBound(params, "shipping_start_datetime");
        String desiredTime = since.isEmpty() || till.isEmpty() ? "" : DeliveryTimeModel.format(since, till, "");
        JsonObject contact = jsonObject.get("contact").getAsJsonObject();
        long clientId = contact.get("id").getAsLong();
        String phone = contact.get("phone").getAsString();
        String name = contact.get("name").getAsString();
        String email = contact.get("email").getAsString();
        String comment = decodeComment(jsonObject);
        ClientDTO client = new ClientDTO(Utils.UNDEFINED_ID, name, null, null,
                phone, email, Utils.UNDEFINED_ID, "", "",
                address, "", false, clientId, null);
        List<OrderPositionDTO> positions = jsonObject.get("items").getAsJsonObject().entrySet()
                .stream()
                .map(this::createPosition)
                .collect(Collectors.toList());
        JsonElement courier = jsonObject.get("courier_info");
        String courierName = courier.isJsonArray() ? null : decodeCourer(courier.getAsJsonObject());
        return OrderDTO.newOrder()
                .id(Utils.UNDEFINED_ID)
                .internetCode(internetCode)
                .clientId(Utils.UNDEFINED_ID)
                .date(creationDate)
                .address(address)
                .managerId(Utils.UNDEFINED_ID)
                .driverId(Utils.UNDEFINED_ID)
                .driver(new StaffDTO(Utils.UNDEFINED_ID, courierName, "", false))
                .desiredTime(desiredTime)
                .deliveryTime(deliveryTime)
                .clientTypeId(Utils.UNDEFINED_ID)
                .orderStateId(Utils.UNDEFINED_ID)
                .phone(phone)
                .possibleGoods("")
                .client(client)
                .description(comment)
                .positions(positions)
                .build();
    }

    private String decodeCourer(JsonObject courier) {
        if (courier == null || courier.isJsonNull())
            return null;
        return courier.get("name").getAsString();
    }

    private String decodeComment(JsonObject jsonObject) {
        try {
            String data = jsonObject.get("comment").getAsString();
            data = data.replaceAll("%(?![0-9a-fA-F]{2})", "%25");
            data = data.replaceAll("\\+", "%2B");
            return URLDecoder.decode(data, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw Utils.logFail(e);
        }
    }

    private OrderPositionDTO createPosition(Map.Entry<String, JsonElement> entry) {
        JsonObject pos = entry.getValue().getAsJsonObject();
        String positionName = pos.get("name").getAsString();
        double price = pos.get("price").getAsDouble();
        double weight = parseWeight(positionName);
        int count = pos.get("quantity").getAsInt();
        String article = pos.get("sku_code").getAsString();
        long skuId = pos.get("sku_id").getAsLong();
        long id = pos.get("id").getAsLong();
        String metadata = MetadataEncoder.encode(id, skuId);
        String articles = ArticlesEncoder.encode(getArticles(pos));
        return OrderPositionDTO.newOrderPosition()
                .id(Utils.UNDEFINED_ID)
                .orderCode(Utils.UNDEFINED_ID)
                .name(positionName)
                .price(price)
                .weight(weight)
                .discount(0d)
                .count(count)
                .article(article)
                .metadata(metadata)
                .additionalArticles(articles)
                .build();
    }

    private ArticlesData getArticles(JsonObject pos) {
        JsonObject articles = pos.get("skus_info").getAsJsonObject().get("providers").getAsJsonObject();
        ArticlesData data = new ArticlesData();
        for (Map.Entry<String, JsonElement> element : articles.entrySet()) {
            JsonObject article = element.getValue().getAsJsonObject();
            String name = article.get("name").getAsString();
            String value = article.get("value").getAsString();
            ArticlesData.Company company = parseCompany(name);
            if (company != null)
                data.setArticle(company, value);
        }
        return data;
    }

    private ArticlesData.Company parseCompany(String name) {
        switch (name) {
            case "Сорсо":
                return ArticlesData.Company.SORSO;
            default:
                return null;
        }
    }

    private double parseWeight(String positionName) {
        Matcher matcher = WEIGHT_PARSER.matcher(positionName);
        if (matcher.find()) {
            return SkuRequest.parseWeight(matcher.group(1));
        }
        return 0;
    }

    private Date getDeliveryDate(JsonObject jsonObject, SimpleDateFormat sdf) {
        try {
            return new Date(sdf.parse(jsonObject.get("shipping_datetime").getAsString()).getTime());
        } catch (Exception e) {
            return new Date(0);
        }
    }

    private String getDesiredTimeBound(JsonObject params, String property) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        SimpleDateFormat hhmm = new SimpleDateFormat("HH:mm");
        try {
            return hhmm.format(sdf.parse(params.get(property).getAsString()));
        } catch (Exception e) {
            return "";
        }
    }
}
