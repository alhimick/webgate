package terra.webasyst.server.requests.products;


import terra.common.api.dto.ProductDTO;
import terra.common.utils.Utils;
import terra.webasyst.Config;
import terra.webasyst.server.ClientFactory;
import terra.webasyst.server.requests.SearchResultExtractor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ProductsCache {
    private final Map<String, List<ProductDTO.Ext>> byName = new ConcurrentHashMap<>();
    private final Map<Long, ProductDTO.Ext> byId = new ConcurrentHashMap<>();
    private final ProductsRequest req = new ProductsRequest(ClientFactory.getTemplate(),
            Config.getInstance().getWebasystHost());

    public ProductsCache() {
        req.setToken(Config.getInstance().getApiKey());
        initCache(this::addToCache);
    }

    private void initCache(Function<Products, Products> action) {
        SearchResultExtractor.extractResult("products", offset -> action.apply(req.getProducts(offset)));
    }

    private Products addToCache(Products products) {
        products.getProducts().forEach(p -> p.getVariants().forEach(v -> byNameList(v).add(v)));
        products.getProducts().forEach(p -> p.getVariants().forEach(v -> byId.put(v.getId(), v)));
        return products;
    }

    public List<ProductDTO.Ext> get(String namePart) {
        return byName.entrySet().stream()
                .filter(e -> e.getKey().toLowerCase().contains(namePart.toLowerCase()))
                .map(Map.Entry::getValue)
                .flatMap(List::stream)
                .collect(Collectors.toList());
    }

    public ProductDTO.Ext get(long skuId) {
        return byId.get(skuId);
    }

    public void update(long productId) {
        if (productId != Utils.UNDEFINED_ID) {
            ProductDTO product = req.getProduct(productId);
            product.getVariants().forEach(v -> replaceInCache(productId, v, byNameList(v)));
        } else {
            initCache(products -> {
                products.getProducts().forEach(product -> product.getVariants().forEach(variant ->
                        replaceInCache(product.getId(), variant, byNameList(variant))
                ));
                return products;
            });
        }
    }

    private List<ProductDTO.Ext> byNameList(ProductDTO.Ext variant) {
        return byName.computeIfAbsent(variant.getName(), k -> new ArrayList<>());
    }

    private void replaceInCache(long extId, ProductDTO.Ext variant, List<ProductDTO.Ext> list) {
        ProductDTO.Ext existing = list.stream().filter(p -> p.getId() == extId).findAny().orElse(null);
        if (existing == null) {
            list.add(variant);
        } else {
            list.remove(existing);
            list.add(variant);
        }
        byId.put(variant.getId(), variant);
    }
}
