package terra.webasyst.server.requests.orders;


import org.apache.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import terra.common.api.dto.OrderDTO;
import terra.common.api.dto.OrderPositionDTO;
import terra.common.api.dto.ProductDTO;
import terra.common.dtedit.DeliveryTimeModel;
import terra.common.utils.Utils;
import terra.webasyst.api.utils.MetadataEncoder;
import terra.webasyst.server.requests.AbstractRequest;
import terra.webasyst.server.requests.products.ProductsCache;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.List;


public class OrderSaveRequest extends AbstractRequest {
    private static final Logger log = Logger.getLogger(OrderSaveRequest.class);
    private static final String POST_REQ = "id=%s&%s&access_token=%s";
    private static final String STATE_REQ = "id=%s&action=%s&access_token=%s";
    private static final String ITEM_TEMPLATE = "items[%s][product_id]=%s&items[%s][sku_id]=%s&items[%s][quantity]=%s";
    private static final String ITEM_TEMPLATE_ID = "&items[%s][id]=%s";
    private final ProductsCache products;


    public OrderSaveRequest(RestTemplate template, String baseUrl, ProductsCache products) {
        super(template, baseUrl);
        this.products = products;
    }

    void updateState(long internetCode, String state) {
        if (state == null || state.isEmpty())
            return;
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(baseUrl + "shop.order.action");
        UriComponents uri = uriBuilder.build().encode();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        String postData = String.format(STATE_REQ, Long.toString(internetCode), state, getToken());
        HttpEntity<String> request = new HttpEntity<>(postData, headers);
        try {
            String result = template.postForObject(uri.toUri(), request, String.class);
            log.info("Save complete with result " + result);
        } catch (Exception e) {
            throw Utils.logFail(e);
        }
    }

    void updateParams(OrderDTO order) {
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(baseUrl + "shop.order.save");
        UriComponents uri = uriBuilder.build().encode();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        String items = encodeItems(order);
        if (items == null || items.isEmpty()) {
            log.warn("No products found for order positions (" + order.getInternetCode() + ")" + order +
                    " skipping sync");
            return;
        }
        long internetCode = OrderService.getInternetCode(order.getInternetCode());
        String postData = String.format(POST_REQ, internetCode, items, getToken()) + encodeComment(order);
        postData += encodeDeliveryTime(order);
        HttpEntity<String> request = new HttpEntity<>(postData, headers);
        String result = template.postForObject(uri.toUri(), request, String.class);
        log.info("Save complete with result " + result);
    }

    private String encodeDeliveryTime(OrderDTO order) {
        StringBuilder result = new StringBuilder();
        DeliveryTimeModel dtm = new DeliveryTimeModel();
        dtm.setLine(order.getDesiredTime());
        if (dtm.isNewStyle()) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String deliveryDate = sdf.format(order.getDeliveryTime());
            result.append("&params[shipping_end_datetime]=").append(deliveryDate).append(" ").append(dtm.getTill())
                    .append(":00");
            result.append("&params[shipping_start_datetime]=").append(deliveryDate).append(" ").append(dtm.getSince())
                    .append(":00");
        }
        return result.toString();
    }

    private String encodeItems(OrderDTO order) {
        List<OrderPositionDTO> positions = order.getPositions();
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < positions.size(); i++) {
            if (result.length() > 0)
                result.append("&");
            OrderPositionDTO position = positions.get(i);
            long skuId = MetadataEncoder.decodeSkuId(position.getMetadata());
            ProductDTO.Ext product = products.get(skuId);
            if (product == null)
                return null;
            String item = String.format(ITEM_TEMPLATE, i, product.getProductId(), i, skuId, i,
                    position.getCount());
            long id = MetadataEncoder.decodeId(position.getMetadata());
            if (id != Utils.UNDEFINED_ID)
                item += String.format(ITEM_TEMPLATE_ID, i, id);
            result.append(item);
        }
        return result.toString();
    }

    private String encodeComment(OrderDTO order) {
        try {
            return "&comment=" + URLEncoder.encode(order.getDescription(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw Utils.logFail(e);
        }
    }
}
