package terra.webasyst.server.requests.orders;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import terra.common.api.dto.OrderDTO;
import terra.webasyst.server.requests.AbstractRequest;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;


public class OrdersRequest extends AbstractRequest {
    private static final ExecutorService EXECUTOR = Executors.newFixedThreadPool(10);
    private final OrderDetailsRequest details;

    public OrdersRequest(RestTemplate template, String baseUrl) {
        super(template, baseUrl);
        details = new OrderDetailsRequest(template, baseUrl);
    }

    @Override
    public void setToken(String token) {
        super.setToken(token);
        details.setToken(token);
    }

    public Orders getOrders(String beforeStataus, int offset) throws InterruptedException, ExecutionException {
        UriComponentsBuilder uriBuilder = UriComponentsBuilder
                .fromHttpUrl(baseUrl + "shop.order.search")
                .queryParam("access_token", getToken())
                .queryParam("offset", offset)
                .queryParam("hash", "search/state_id=" + beforeStataus);
        UriComponents uri = uriBuilder.build().encode();
        String line = template.getForObject(uri.toUri(), String.class);
        JsonObject jsonObject = new JsonParser().parse(line).getAsJsonObject();
        JsonArray ordersArray = jsonObject.get("orders").getAsJsonArray();

        List<Callable<OrderDTO>> tasks = new ArrayList<>();
        ordersArray.forEach(e -> tasks.add(() -> {
            JsonObject obj = e.getAsJsonObject();
            long id = obj.get("id").getAsLong();
            return details.getOrderDetails(id);
        }));

        List<Future<OrderDTO>> futures = EXECUTOR.invokeAll(tasks);
        List<OrderDTO> orders = new ArrayList<>();
        for (Future<OrderDTO> future : futures) {
            orders.add(future.get());
        }
        return new Orders(orders, jsonObject.get("count").getAsInt(), offset, jsonObject.get("limit").getAsInt());
    }
}
