package terra.webasyst.server.requests.products;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import terra.common.api.dto.ProductDTO;
import terra.webasyst.server.requests.AbstractRequest;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;


public class ProductsRequest extends AbstractRequest {
    private static final ExecutorService EXECUTOR = Executors.newFixedThreadPool(10);
    private final SkuRequest skus;

    public ProductsRequest(RestTemplate template, String baseUrl) {
        super(template, baseUrl);
        skus = new SkuRequest(template, baseUrl);
    }

    public Products getProducts(int offset) throws InterruptedException, ExecutionException {
        UriComponentsBuilder uriBuilder = UriComponentsBuilder
                .fromHttpUrl(baseUrl + "shop.product.search")
                .queryParam("access_token", getToken())
                .queryParam("offset", offset);
        UriComponents uri = uriBuilder.build().encode();
        String line = template.getForObject(uri.toUri(), String.class);
        JsonObject jsonObject = new JsonParser().parse(line).getAsJsonObject();
        List<Callable<ProductDTO>> tasks = new ArrayList<>();
        jsonObject.getAsJsonArray("products").forEach(e -> tasks.add(() -> {
            JsonObject obj = e.getAsJsonObject();
            long id = obj.get("id").getAsLong();
            return getProduct(obj.get("name").getAsString(), id);
        }));
        List<Future<ProductDTO>> futures = EXECUTOR.invokeAll(tasks);
        List<ProductDTO> products = new ArrayList<>();
        for (Future<ProductDTO> future : futures) {
            products.add(future.get());
        }
        return new Products(products, jsonObject.get("count").getAsInt(), offset, jsonObject.get("limit").getAsInt());
    }

    private ProductDTO getProduct(String name, long id) {
        List<ProductDTO.Ext> modifications = skus.getSkuList(id, name);
        return new ProductDTO(id, name, modifications);
    }

    public ProductDTO getProduct(long id) {
        UriComponentsBuilder uriBuilder = UriComponentsBuilder
                .fromHttpUrl(baseUrl + "shop.product.getInfo")
                .queryParam("access_token", getToken())
                .queryParam("id", id);
        UriComponents uri = uriBuilder.build().encode();
        String line = template.getForObject(uri.toUri(), String.class);
        JsonObject jsonObject = new JsonParser().parse(line).getAsJsonObject();
        String name = jsonObject.get("name").getAsString();
        return getProduct(name, id);
    }

    @Override
    public void setToken(String token) {
        super.setToken(token);
        skus.setToken(token);
    }
}
