package terra.webasyst.server.requests.orders;


import terra.common.api.dto.OrderDTO;
import terra.webasyst.server.requests.SearchResult;

import java.util.List;


public class Orders extends SearchResult {
    private final List<OrderDTO> orders;

    public Orders(List<OrderDTO> orders, int count, int offset, int limit) {
        super(count, offset, limit);
        this.orders = orders;
    }

    public List<OrderDTO> getOrders() {
        return orders;
    }
}
