package terra.webasyst.server.requests.products;

import terra.common.api.dto.ProductDTO;
import terra.webasyst.server.requests.SearchResult;

import java.util.List;


public class Products extends SearchResult {
    private final List<ProductDTO> products;

    public Products(List<ProductDTO> products, int count, int offset, int limit) {
        super(count, offset, limit);
        this.products = products;
    }

    public List<ProductDTO> getProducts() {
        return products;
    }
}
