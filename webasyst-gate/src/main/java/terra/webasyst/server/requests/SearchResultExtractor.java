package terra.webasyst.server.requests;


import org.apache.log4j.Logger;
import terra.common.utils.TimeUtils;
import terra.common.utils.Utils;


public class SearchResultExtractor {
    private static final Logger log = Logger.getLogger(SearchResultExtractor.class);

    private SearchResultExtractor() {
    }

    public static void extractResult(String description, Extractor extractor) {
        log.info("Loading " + description);
        long start = System.currentTimeMillis();
        SearchResult result = null;
        do {
            try {
                int offset = result == null ? 0 : (result.getOffset() + result.getLimit());
                result = extractor.apply(offset);
            } catch (Exception e) {
                log.error(e, e);
                Utils.sleep(TimeUtils.MIN);
            }
        } while (result == null || result.getOffset() < result.getCount());
        long finish = System.currentTimeMillis();
        log.info("Loading complete in " + (finish - start) / (TimeUtils.MIN) + " min");
    }

    @FunctionalInterface
    public interface Extractor {
        SearchResult apply(int offset) throws Exception;
    }
}
