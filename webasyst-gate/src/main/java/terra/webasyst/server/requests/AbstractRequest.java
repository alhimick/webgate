package terra.webasyst.server.requests;


import org.springframework.web.client.RestTemplate;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;

public abstract class AbstractRequest {
    protected final RestTemplate template;
    protected final String baseUrl;
    private final AtomicReference<String> tokenHolder = new AtomicReference<>();

    public AbstractRequest(RestTemplate template, String baseUrl) {
        this.template = template;
        this.baseUrl = baseUrl;
    }

    protected String getToken() {
        String token = tokenHolder.get();
        Objects.requireNonNull(token);
        return token;
    }

    public void setToken(String token) {
        tokenHolder.set(token);
    }
}
