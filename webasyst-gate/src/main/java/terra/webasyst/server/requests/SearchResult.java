package terra.webasyst.server.requests;


public abstract class SearchResult {
    private final int count;
    private final int offset;
    private final int limit;

    public SearchResult(int count, int offset, int limit) {
        this.count = count;
        this.offset = offset;
        this.limit = limit;
    }

    public int getCount() {
        return count;
    }

    public int getOffset() {
        return offset;
    }

    public int getLimit() {
        return limit;
    }
}
