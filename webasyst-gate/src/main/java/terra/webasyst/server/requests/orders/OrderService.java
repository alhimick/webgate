package terra.webasyst.server.requests.orders;


import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import terra.common.api.dto.OrderDTO;
import terra.common.utils.Utils;
import terra.webasyst.Config;
import terra.webasyst.api.exceptions.WebGateException;
import terra.webasyst.api.exceptions.WebOrderNotFoundException;
import terra.webasyst.server.ClientFactory;
import terra.webasyst.server.requests.SearchResultExtractor;
import terra.webasyst.server.requests.products.ProductsCache;

import java.util.ArrayList;
import java.util.List;


public class OrderService {
    private final RestTemplate template = ClientFactory.getTemplate();
    private final OrdersRequest dataReq = new OrdersRequest(template, Config.getInstance().getWebasystHost());
    private final OrderSaveRequest saveReq;

    public OrderService(ProductsCache products) {
        String key = Config.getInstance().getApiKey();
        dataReq.setToken(key);
        saveReq = new OrderSaveRequest(template, Config.getInstance().getWebasystHost(), products);
        saveReq.setToken(key);
    }

    static long getInternetCode(String code) {
        return Long.parseLong(code.replaceFirst(Config.getInstance().getIdPrefix(), ""));
    }

    public List<OrderDTO> getNewOrders(String beforeStataus) {
        List<OrderDTO> result = new ArrayList<>();
        SearchResultExtractor.extractResult("orders", (offset) -> {
            Orders orders = dataReq.getOrders(beforeStataus, offset);
            result.addAll(orders.getOrders());
            return orders;
        });
        return result;
    }

    public void updateOrder(OrderDTO order) {
        if (!Config.getInstance().isProd())
            return;
        if (!order.isInternet()) {
            throw new IllegalArgumentException("Order is not internet order " + order);
        }
        try {
            updateStatus(order);
            updateOrderData(order);
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode() == HttpStatus.NOT_FOUND) {
                throw new WebOrderNotFoundException("Web order not found: " + order.getInternetCode());
            } else {
                throw new WebGateException(e);
            }
        }
    }

    private void updateStatus(OrderDTO order) {
        String code = order.getInternetCode();
        long internetId = getInternetCode(code);
        saveReq.updateState(internetId, order.getState().getInternetCode());
    }

    private void updateOrderData(OrderDTO order) {
        try {
            saveReq.updateParams(order);
        } catch (Exception e) {
            throw Utils.logFail(e);
        }
    }
}
