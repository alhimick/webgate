package terra.webasyst.server;


import org.apache.log4j.Logger;
import terra.common.api.dto.OrderDTO;
import terra.common.api.dto.ProductDTO;
import terra.webasyst.api.exceptions.WebGateException;
import terra.webasyst.api.services.WebasystService;
import terra.webasyst.server.requests.orders.OrderService;
import terra.webasyst.server.requests.products.ProductsCache;

import java.util.List;

public class WebasystServiceImpl implements WebasystService {
    private static final Logger log = Logger.getLogger(WebasystServiceImpl.class);
    private final ProductsCache products = new ProductsCache();
    private final OrderService orders = new OrderService(products);


    @Override
    public List<ProductDTO.Ext> getProducts(String namePart) {
        return products.get(namePart);
    }

    @Override
    public void updateProduct(long id) {
        try {
            products.update(id);
        } catch (Exception e) {
            log.error(e, e);
            throw new WebGateException(e.getMessage());
        }
    }

    @Override
    public List<OrderDTO> getOrders(String beforeStatus) {
        return orders.getNewOrders(beforeStatus);
    }

    @Override
    public void updateOrder(OrderDTO order) {
        orders.updateOrder(order);
    }
}
