package terra.webasyst.server;

import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.web.client.RestTemplate;
import terra.webasyst.Config;

public class ClientFactory {
    private ClientFactory() {
    }

    public static RestTemplate getTemplate() {
        RestTemplate client = new RestTemplate();
        String auth = Config.getInstance().getAuth();
        if (Config.getInstance().getAuth() != null) {
            String[] authData = auth.split(":");
            client.getInterceptors().add(new BasicAuthorizationInterceptor(authData[0], authData[1]));
        }
        return client;
    }
}
