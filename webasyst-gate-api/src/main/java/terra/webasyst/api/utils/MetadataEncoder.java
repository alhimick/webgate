package terra.webasyst.api.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import terra.common.utils.Utils;

import java.util.Optional;

public class MetadataEncoder {
    private static final Gson GSON = new GsonBuilder().create();

    private MetadataEncoder() {
    }

    public static String encode(Long inetId, Long skuId) {
        Metadata md = new Metadata().setId(inetId).setSkuId(skuId);
        return GSON.toJson(md);
    }

    public static long decodeId(String metadata) {
        Metadata md = GSON.fromJson(metadata, Metadata.class);
        return Optional.ofNullable(md).map(Metadata::getId).orElse(Utils.UNDEFINED_ID);
    }

    public static long decodeSkuId(String metadata) {
        Metadata md = GSON.fromJson(metadata, Metadata.class);
        return Optional.ofNullable(md).map(Metadata::getSkuId).orElse(Utils.UNDEFINED_ID);
    }
}
