package terra.webasyst.api.utils;

public class Metadata {
    private Long skuId;
    private Long id;

    public Long getSkuId() {
        return skuId;
    }

    public Metadata setSkuId(Long skuId) {
        this.skuId = skuId;
        return this;
    }

    public Long getId() {
        return id;
    }

    public Metadata setId(Long id) {
        this.id = id;
        return this;
    }

    @Override
    public String toString() {
        return "Metadata{" +
                "skuId=" + skuId +
                ", id=" + id +
                '}';
    }
}
