package terra.webasyst.api.exceptions;


public class WebOrderNotFoundException extends WebGateException {
    public WebOrderNotFoundException(String desc) {
        super(desc);
    }
}
