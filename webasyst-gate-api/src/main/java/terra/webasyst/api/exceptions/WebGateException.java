package terra.webasyst.api.exceptions;

import terra.common.exceptions.NoRollBackException;

public class WebGateException extends NoRollBackException {
    public WebGateException(Throwable cause) {
        super(cause);
    }

    public WebGateException(String message) {
        super(message);
    }
}
