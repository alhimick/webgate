package terra.webasyst.api.services;


import terra.common.api.dto.OrderDTO;
import terra.common.api.dto.ProductDTO;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface WebasystService extends Remote {
    String DESCRIPTOR = "webasyst";

    List<ProductDTO.Ext> getProducts(String namePart) throws RemoteException;

    void updateProduct(long id) throws RemoteException;

    List<OrderDTO> getOrders(String status) throws RemoteException;

    void updateOrder(OrderDTO order) throws RemoteException;
}
