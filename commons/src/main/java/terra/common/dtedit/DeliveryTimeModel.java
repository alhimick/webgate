package terra.common.dtedit;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DeliveryTimeModel {
    private static final Pattern STYLE_PATTERN = Pattern.compile("(\\d\\d:00) - (\\d\\d:00);(.*)");
    private static final String STYLE_FORMAT = "%s - %s;%s";
    private String line;
    private Matcher matcher;

    public static String format(String since, String till, String description) {
        String notNullDescription = description == null ? "" : description;
        return String.format(STYLE_FORMAT, since, till, notNullDescription);
    }

    public void setLine(String line) {
        this.line = line;
        matcher = STYLE_PATTERN.matcher(line == null ? "" : line);
    }

    public boolean isNewStyle() {
        return line == null || matcher.matches();
    }

    public String getSince() {
        return getPartOrDefault(1, "");
    }

    public String getTill() {
        return getPartOrDefault(2, "");
    }

    public String getDescription() {
        return getPartOrDefault(3, line);
    }

    private String getPartOrDefault(int group, String defaultLine) {
        return isNewStyle() ? matcher.group(group) : defaultLine;
    }
}
