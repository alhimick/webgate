package terra.common.export;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import terra.common.api.dto.reports.BillReport;

import javax.swing.table.TableModel;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static terra.common.export.ReportUtils.addRow;


public class BillExporter {

    public static Workbook exportBill(BillReport bill) {
        Workbook book = new XSSFWorkbook();
        Sheet sheet = book.createSheet();
        Map<String, Object> params = bill.getFields();
        addRow(sheet, 0, new int[]{0, 2, 7, 9}, new String[]{"Товарный чек", "574-72-11", "Клиент", (String) params.get("client")});
        addRow(sheet, 1, new int[]{0, 7, 9}, new String[]{"ООО 'ТЕРА' Юр. адрес: 193312 ул. Коллонтай, д.31 к.2", "Телефон", (String) params.get("phone")});
        addRow(sheet, 2, new int[]{0, 7, 9}, new String[]{"ИНН 7811383371 КПП 781101001", "Адрес", (String) params.get("address")});
        addRow(sheet, 4, new int[]{7, 9}, new Object[]{"Дата доставки", (Date) params.get("deliveryDate")});
        String orderCode = (String) params.get("orderCode");
        if (!orderCode.contains("-"))
            orderCode += "-";
        addRow(sheet, 5, new int[]{0, 2, 7, 9}, new String[]{"Заказ номер", orderCode, "Желаемое время", (String) params.get("desiredTime")});
        addRow(sheet, 7, new int[]{0, 1, 7, 8, 9, 10}, new String[]{"Артикул", "Наименование товара", "цена", "шт", "скидка", "стоимость"});
        int positionIndex = 8;
        TableModel model = bill.getTableModel();
        Map<Integer, CellValueParser> parsers = new HashMap<>();
        parsers.put(0, CellValueParser.STRING);
        for (int i = 0; i < model.getRowCount(); i++) {
            String cost = (model.getValueAt(i, 5)).equals("0") ? "0.01" : ((String) model.getValueAt(i, 5)); // hack for 1C
            addRow(sheet, positionIndex++, new int[]{0, 1, 7, 8, 9, 10},
                    new String[]{(String) model.getValueAt(i, 0), (String) model.getValueAt(i, 1),
                            (String) model.getValueAt(i, 2), (String) model.getValueAt(i, 3),
                            (String) model.getValueAt(i, 4), cost}, parsers);
        }
        positionIndex++;
        addRow(sheet, ++positionIndex, new int[]{0, 3}, new String[]{"Стоимость заказа", ((String) params.get("totalCost")).replace(" р.", "")});
        addRow(sheet, ++positionIndex, new int[]{0, 3}, new String[]{"Стоимость прописью", (String) params.get("totalCostText")});
        addRow(sheet, ++positionIndex, new int[]{0, 3}, new String[]{"Продавец-консультант", (String) params.get("manager")});
        addRow(sheet, ++positionIndex, new int[]{0, 3}, new String[]{"Водитель", (String) params.get("driver")});
        return book;
    }
}
