package terra.common.export;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;


public class ReportUtils {
    public static void addRow(Sheet sheet, int rowIndex, List<?> values) {
        int[] indexes = new int[values.size()];
        Object[] valuesArray = new Object[values.size()];
        for (int i = 0; i < values.size(); i++) {
            indexes[i] = i;
            valuesArray[i] = values.get(i);
        }
        addRow(sheet, rowIndex, indexes, valuesArray);
    }

    public static void addRow(Sheet sheet, int rowIndex, int[] indexes, Object[] values) {
        addRow(sheet, rowIndex, indexes, values, Collections.emptyMap());
    }

    public static void addRow(Sheet sheet, int rowIndex, int[] indexes, Object[] values,
                              Map<Integer, CellValueParser> parsers) {
        if (indexes.length != values.length)
            throw new IllegalArgumentException("Different lengths!");
        Row row = sheet.createRow(rowIndex);
        for (int i = 0; i < indexes.length; i++) {
            Cell cell = row.createCell(indexes[i]);
            String cellValue = values[i] == null ? "" : values[i].toString();
            if (parsers.get(i) != null) {
                parsers.get(i).setCellValue(cell, values[i]);
                continue;
            }
            try {
                CellValueParser.DIGIT.setCellValue(cell, cellValue);
            } catch (NumberFormatException ignore) {
                Object value = values[i];
                if (value instanceof Date) {
                    CellValueParser.DATE.setCellValue(cell, value);
                } else {
                    cell.setCellValue(cellValue);
                }
            }
        }
    }
}
