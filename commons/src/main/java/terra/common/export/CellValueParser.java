package terra.common.export;


import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;

import java.util.Date;

public enum CellValueParser {
    STRING {
        @Override
        public void setCellValue(Cell cell, Object value) {
            cell.setCellValue(value.toString());
        }
    },
    DIGIT {
        @Override
        public void setCellValue(Cell cell, Object value) {
            double digitValue = Double.parseDouble(value.toString());
            cell.setCellValue(digitValue);
        }
    },
    DATE {
        @Override
        public void setCellValue(Cell cell, Object value) {
            setDateCell(cell, value, "dd.mm.yyyy");
        }
    },
    DATE_HH_MM {
        @Override
        public void setCellValue(Cell cell, Object value) {
            setDateCell(cell, value, "dd.mm.yy hh:mm");
        }
    };

    public abstract void setCellValue(Cell cell, Object value);

    protected void setDateCell(Cell cell, Object value, String format) {
        CellStyle cellStyle = cell.getSheet().getWorkbook().createCellStyle();
        CreationHelper createHelper = cell.getSheet().getWorkbook().getCreationHelper();
        cellStyle.setDataFormat(createHelper.createDataFormat().getFormat(format));
        cell.setCellValue((Date) value);
        cell.setCellStyle(cellStyle);
    }
}
