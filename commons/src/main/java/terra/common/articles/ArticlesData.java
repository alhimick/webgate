package terra.common.articles;

import java.util.EnumMap;
import java.util.Map;

public class ArticlesData {
    private Map<Company, String> articles = new EnumMap<>(Company.class);

    public ArticlesData setArticle(Company company, String article) {
        articles.put(company, article);
        return this;
    }

    public String getArticle(Company company) {
        return articles.get(company);
    }

    @Override
    public String toString() {
        return "ArticlesData{" +
                "articles=" + articles +
                '}';
    }

    public enum Company {SORSO}
}
