package terra.common.articles;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Optional;

public class ArticlesEncoder {
    private static final Gson GSON = new GsonBuilder().create();

    private ArticlesEncoder() {
    }

    public static String encode(ArticlesData data) {
        return GSON.toJson(data);
    }

    public static String decode(String data, ArticlesData.Company company) {
        return decode(data).map(d -> d.getArticle(company)).orElse("");
    }

    private static Optional<ArticlesData> decode(String data) {
        return Optional.ofNullable(GSON.fromJson(data, ArticlesData.class));
    }
}
