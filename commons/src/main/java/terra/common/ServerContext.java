package terra.common;


import terra.common.api.dto.login.LoginDTO;
import terra.common.api.dto.login.LoginReplyDTO;
import terra.common.api.methods.MethodDescriptor;
import terra.common.api.methods.SessionsDescriptor;
import terra.common.api.permissions.AuthType;
import terra.common.api.services.ServiceDescriptors;
import terra.common.api.services.invoker.InvocationContext;
import terra.common.api.services.invoker.InvokerService;
import terra.common.utils.AuthUtils;
import terra.common.utils.Utils;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class ServerContext {
    private final InvokerService invoker;
    private final InvocationContext ic;

    public ServerContext(String host, int port, String login, String password) throws Exception {
        Registry registry = LocateRegistry.getRegistry(host, port);
        invoker = (InvokerService) registry.lookup(ServiceDescriptors.INVOKER.getDescriptor());
        LoginDTO loginDTO = new LoginDTO(AuthUtils.getVersion(), login,
                AuthUtils.sha256(password), AuthType.LOGIN);
        Object result = invoker.invoke(null, SessionsDescriptor.LOGIN, new Object[]{loginDTO});
        ic = new InvocationContext(AuthUtils.getComputerName(), ((LoginReplyDTO) result).getSessionId());
    }

    @SuppressWarnings("unchecked")
    public <T> T invoke(MethodDescriptor descriptor, Object... args) {
        try {
            return (T) invoker.invoke(ic, descriptor, args);
        } catch (RemoteException e) {
            throw Utils.logFail(e);
        }
    }
}
